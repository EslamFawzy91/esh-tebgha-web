(function($) {
	"use strict";

	$('#ad_catsdiv').hide();
	$('#ad_tagsdiv').hide();
	$('#ad_conditiondiv').hide();
	$('#ad_typediv').hide();
	$('#ad_warrantydiv').hide();
	$('#ad_currencydiv').hide();
	$('#ad_countrydiv').hide();
	
})( jQuery );
function adforest_fresh_install()
{
		 var is_fresh_copy =	confirm("Are you installating it with fresh copy of WordPress? Please only select OK if it is fresh installation.");
		 if( is_fresh_copy )
		 {
			jQuery.ajax({
				type: "POST",
				url: ajaxurl,
				data: { action: 'demo_data_start' , is_fresh: 'yes' }
				}).done(function( msg ) {
				//alert( msg );
			});
			
		 }
}
jQuery('.get_user_meta_id').on('click', function()
{
	is_process	=	confirm("Are you sure to delete it.");
	if( is_process )
	{
		meta_id	=	 jQuery(this).attr('data-mid');	
		jQuery.ajax({
			type: "POST",
			url: ajaxurl,
			data: { action: 'sb_delete_user_rating' , meta_id: meta_id }
			}).done(function( data ) {
			if( data == "1" )
			{
				location.reload();
			}
		});

	}
});

jQuery('.bids-in-admin').on('click', function()
{
	is_process	=	confirm("Are you sure to delete it.");
	if( is_process )
	{
		meta_id	=	 jQuery(this).attr('data-bid-meta');	
		jQuery.ajax({
			type: "POST",
			url: ajaxurl,
			data: { action: 'sb_delete_user_bid' , meta_id: meta_id }
			}).done(function( data ) {
			if( data == "1" )
			{
				location.reload();
			}
		});

	}
});


