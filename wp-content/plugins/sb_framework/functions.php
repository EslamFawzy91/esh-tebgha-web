<?php
function adforest_add_code( $id, $func )
{
	add_shortcode( $id, $func );
}

function adforest_decode( $html )
{
	return base64_decode ( $html );	
}

// Ajax handler for add to cart
add_action( 'wp_ajax_sb_mailchimp_subcribe', 'adforest_mailchimp_subcribe' );
add_action( 'wp_ajax_nopriv_sb_mailchimp_subcribe', 'adforest_mailchimp_subcribe' );
// Addind Subcriber into Mailchimp
function adforest_mailchimp_subcribe()
{
	global $adforest_theme;
	
	//$apiKey = '97da4834058c44cd770dbbdbab0c5730-us14';
	// $listid = '9b80a80904';
		$sb_action	=	$_POST['sb_action'];
	
		$apiKey	=	$adforest_theme['mailchimp_api_key'];
		
		if( $sb_action == 'coming_soon' )
			$listid =	$adforest_theme['mailchimp_notify_list_id'];
		if( $sb_action == 'footer_action' )
			$listid =	$adforest_theme['mailchimp_footer_list_id'];
			
			if( $apiKey == "" || $listid == "" )
			{
				echo 0;
				die();
			}
		// Getting value from form
		$email	=	$_POST['sb_email'];
		$fname	=	'';
		$lname	=	'';
		
        // MailChimp API URL
        $memberID = md5(strtolower($email));
        $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
        $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listid . '/members/' . $memberID;
        
        // member information
        $json = json_encode(array(
            'email_address' => $email,
            'status'        => 'subscribed',
            'merge_fields'  => array(
                'FNAME'     => $fname,
                'LNAME'     => $lname
            )
        ));
        
        // send a HTTP POST request with curl
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        // store the status message based on response code
		
		$mcdata=json_decode($result);
		if (!empty($mcdata->error))
		{
			echo 0;
		}
		else
		{
			echo 1;
		}
	die();
}

// Report Ad
add_action('wp_ajax_sb_report_ad', 'adforest_sb_report_ad');
add_action( 'wp_ajax_nopriv_sb_report_ad', 'adforest_sb_report_ad' );
function adforest_sb_report_ad()
{
	adforest_authenticate_check();
	
	global $adforest_theme;
	$ad_id		=	$_POST['ad_id'];
	$option		=	$_POST['option'];
	$comments	=	sanitize_text_field($_POST['comments']);
	if( get_post_meta( $ad_id, '_sb_user_id_' .get_current_user_id() , true ) == get_current_user_id() )
	{
		echo '0|' . __( "You have reported already.", 'redux-framework' );
	}
	else
	{
		update_post_meta( $ad_id, '_sb_user_id_' . get_current_user_id(), get_current_user_id() );
		update_post_meta( $ad_id, '_sb_report_option_' . get_current_user_id(), $option );
		update_post_meta( $ad_id, '_sb_report_comments_' . get_current_user_id(), $comments );
		
		$count	=	get_post_meta( $ad_id, '_sb_count_report', true );
		$count	=	$count + 1;
		update_post_meta( $ad_id, '_sb_count_report', $count );
		if( $count >= $adforest_theme['report_limit'] )
		{
			if( $adforest_theme['report_action'] == '1' )
			{
				$my_post = array(
				'ID'           => $ad_id,
				'post_status'   => 'pending',
				);
				
				wp_update_post( $my_post );
	
			}
			else
			{
						// Sending email
				$to = $adforest_theme['report_email'];
				$subject = __('Ad Reported', 'redux-framework');
				$body = '<html><body><p>'.__('Users reported this ad, please check it. ','redux-framework').'<a href="'.get_the_permalink( $ad_id ).'">' . get_the_title( $ad_id )  . '</a></p></body></html>';
				
		$from	=	get_bloginfo( 'name' );
		if( isset( $adforest_theme['sb_report_ad_from'] ) && $adforest_theme['sb_report_ad_from'] != "" )
		{
			$from	=	$adforest_theme['sb_report_ad_from'];
		}
		$headers = array('Content-Type: text/html; charset=UTF-8',"From: $from" );
		if( isset( $adforest_theme['sb_report_ad_message'] ) &&  $adforest_theme['sb_report_ad_message'] != "" )
		{
			
			
			$subject_keywords  = array('%site_name%', '%ad_title%');
			$subject_replaces  = array(get_bloginfo( 'name' ),  get_the_title($ad_id));
			
			$subject = str_replace($subject_keywords, $subject_replaces, $adforest_theme['sb_report_ad_subject']);

			$author_id = get_post_field ('post_author', $ad_id);
			$user_info = get_userdata($author_id);
			
			$msg_keywords  = array('%site_name%', '%ad_title%', '%ad_link%', '%ad_owner%');
			$msg_replaces  = array(get_bloginfo( 'name' ),  get_the_title($ad_id), get_the_permalink($ad_id), $user_info->display_name  );
			
			$body = str_replace($msg_keywords, $msg_replaces, $adforest_theme['sb_report_ad_message']);

		}

				
				wp_mail( $to, $subject, $body, $headers );
			}
		}
		
		echo '1|' . __( "Reported successfully.", 'redux-framework' );
	}

die();

}

// Send message to ad owner
add_action('wp_ajax_sb_send_message', 'adforest_send_message');

function adforest_send_message()
{
	adforest_authenticate_check();
	// Getting values
	$params = array();
    parse_str($_POST['sb_data'], $params);
	
	$time = current_time('mysql');
	

$data = array(
    'comment_post_ID' => $params['ad_post_id'],
    'comment_author' => $params['name'],
    'comment_author_email' => $params['email'],
    'comment_author_url' => '',
    'comment_content' => sanitize_text_field($params['message']),
    'comment_type' => 'ad_post',
    'comment_parent' => $params['usr_id'],
    'user_id' => get_current_user_id(),
    'comment_author_IP' => $_SERVER['REMOTE_ADDR'],
    'comment_date' => $time,
	'comment_approved' => 1,
);

	global $adforest_theme;
	if( $adforest_theme['sb_send_email_on_message'] )
	{
		$author_obj = get_user_by('id', $params['msg_receiver_id']);
		$to = $author_obj->user_email;
		$subject = __('New Message', 'redux-framework');
		$body = '<html><body><p>'.__('Got new message on ad','redux-framework'). ' ' . get_the_title( $params['ad_post_id'] ) .'</p><p>'.$params['message'].'</p></body></html>';
		$from	=	get_bloginfo( 'name' );
		if( isset( $adforest_theme['sb_message_from_on_new_ad'] ) && $adforest_theme['sb_message_from_on_new_ad'] != "" )
		{
			$from	=	$adforest_theme['sb_message_from_on_new_ad'];
		}
		$headers = array('Content-Type: text/html; charset=UTF-8',"From: $from" );
		if( isset( $adforest_theme['sb_message_on_new_ad'] ) &&  $adforest_theme['sb_message_on_new_ad'] != "" )
		{
			
			
			$subject_keywords  = array('%site_name%', '%ad_title%');
			$subject_replaces  = array(get_bloginfo( 'name' ),  get_the_title($params['ad_post_id']));
			
			$subject = str_replace($subject_keywords, $subject_replaces, $adforest_theme['sb_message_subject_on_new_ad']);

			$msg_keywords  = array('%site_name%', '%ad_title%', '%ad_link%', '%message%', '%sender_name%');
			$msg_replaces  = array(get_bloginfo( 'name' ),  get_the_title($params['ad_post_id']), get_the_permalink($params['ad_post_id']), $params['message'], $params['name']   );
			
			$body = str_replace($msg_keywords, $msg_replaces, $adforest_theme['sb_message_on_new_ad']);

		}
		wp_mail( $to, $subject, $body, $headers );
	
	}
	$comment_id	=	wp_insert_comment($data);
	if( $comment_id )
	{
		if( function_exists( 'adforestAPI_messages_sent_func' ) )
		{
			adforestAPI_messages_sent_func( 'sent',  $params['msg_receiver_id'], get_current_user_id(), $params['usr_id'], $comment_id, $params['ad_post_id'], sanitize_text_field($params['message']), $time );
		}
		
		update_comment_meta( $params['msg_receiver_id'], $params['ad_post_id']."_".get_current_user_id(), 0 );
		echo '1|' . __( "Message sent successfully .", 'redux-framework' );	
	}
	else
	{
		echo '0|' . __( "Message not sent, please try again later.", 'redux-framework' );
	}
	die();

	
}

// Ajax handler for Forgot Password
add_action( 'wp_ajax_sb_forgot_password', 'adforest_forgot_password' );
add_action( 'wp_ajax_nopriv_sb_forgot_password', 'adforest_forgot_password' );
// Forgot Password
function adforest_forgot_password()
{
	global $adforest_theme;
	// Getting values
	$params = array();
    parse_str($_POST['sb_data'], $params);
	$email	=	$params['sb_forgot_email'];
	if( email_exists( $email ) == true )
	{
		$from	=	get_bloginfo( 'name' );	
		if( isset( $adforest_theme['sb_forgot_password_from'] ) && $adforest_theme['sb_forgot_password_from'] != "" )
		{
			$from	=	$adforest_theme['sb_forgot_password_from'];
		}
		$headers = array('Content-Type: text/html; charset=UTF-8',"From: $from" );
		if( isset( $adforest_theme['sb_forgot_password_message'] ) &&  $adforest_theme['sb_forgot_password_message'] != "" )
		{
			
			
			$subject_keywords  = array('%site_name%');
			$subject_replaces  = array(get_bloginfo( 'name' ));
			
			$subject = str_replace($subject_keywords, $subject_replaces, $adforest_theme['sb_forgot_password_subject']);

			$token 	=	 adforest_randomString(50);
			
			$user = get_user_by( 'email', $email );
			$msg_keywords  = array('%site_name%', '%user%', '%reset_link%');
			$reset_link	=	trailingslashit( get_home_url() ) . '?token=' . $token . '-sb-uid-'. $user->ID;
			$msg_replaces  = array(get_bloginfo( 'name' ),  $user->display_name, $reset_link );
			
			$body = str_replace($msg_keywords, $msg_replaces, $adforest_theme['sb_forgot_password_message']);
			
			$to = $email;
			$mail = wp_mail( $to, $subject, $body, $headers );
			if( $mail )
			{
				update_user_meta($user->ID, 'sb_password_forget_token', $token);
				echo "1";
			}
			else
			{
				echo __( 'Email server not responding', 'redux-framework' );	
			}


		}
	}
	else
	{
		echo __( 'Email is not resgistered with us.', 'redux-framework' );	
	}
	die();
}

function adforest_get_notify_on_ad_post($pid)
{
	global $adforest_theme;
	if( isset( $adforest_theme['sb_send_email_on_ad_post'] ) && $adforest_theme['sb_send_email_on_ad_post'] )
	{
		$to = $adforest_theme['ad_post_email_value'];
		$subject = __('New Ad', 'redux-framework') . '-' . get_bloginfo( 'name' );
		$body = '<html><body><p>'.__('Got new ad','redux-framework'). ' <a href="'.get_edit_post_link($pid).'">' . get_the_title($pid) .'</a></p></body></html>';
		$from	=	get_bloginfo( 'name' );
		if( isset( $adforest_theme['sb_msg_from_on_new_ad'] ) && $adforest_theme['sb_msg_from_on_new_ad'] != "" )
		{
			$from	=	$adforest_theme['sb_msg_from_on_new_ad'];
		}
			$headers = array('Content-Type: text/html; charset=UTF-8',"From: $from" );
		if( isset( $adforest_theme['sb_msg_on_new_ad'] ) &&  $adforest_theme['sb_msg_on_new_ad'] != "" )
		{
			
			$author_id = get_post_field ('post_author', $pid);
			$user_info = get_userdata($author_id);
			
			$subject_keywords  = array('%site_name%', '%ad_owner%', '%ad_title%');
			$subject_replaces  = array(get_bloginfo( 'name' ), $user_info->display_name, get_the_title($pid));
			
			$subject = str_replace($subject_keywords, $subject_replaces, $adforest_theme['sb_msg_subject_on_new_ad']);

			$msg_keywords  = array('%site_name%', '%ad_owner%', '%ad_title%', '%ad_link%');
			$msg_replaces  = array(get_bloginfo( 'name' ), $user_info->display_name, get_the_title($pid), get_the_permalink($pid) );
			
			$body = str_replace($msg_keywords, $msg_replaces, $adforest_theme['sb_msg_on_new_ad']);

		}
		wp_mail( $to, $subject, $body, $headers );

	
	}
}
function adforest_send_email_new_rating( $sender_id, $receiver_id, $rating = '', $comments = '' )
{
	global $adforest_theme;
		$receiver_info = get_userdata($receiver_id);
		$to = $receiver_info->user_email;
		$subject = __('New Rating', 'redux-framework') . '-' . get_bloginfo( 'name' );
		
		$body = '<html><body><p>'.__('Got new Rating','redux-framework'). ' <a href="'.get_author_posts_url( $receiver_id ).'?type=1">' .get_author_posts_url( $receiver_id ).'</a></p></body></html>';
		$from	=	get_bloginfo( 'name' );
		
		if( isset( $adforest_theme['sb_new_rating_from'] ) && $adforest_theme['sb_new_rating_from'] != "" )
		{
			$from	=	$adforest_theme['sb_new_rating_from'];
		}
			$headers = array('Content-Type: text/html; charset=UTF-8',"From: $from" );
		if( isset( $adforest_theme['sb_new_rating_message'] ) &&  $adforest_theme['sb_new_rating_message'] != "" )
		{
			
			
			
			$subject_keywords  = array('%site_name%');
			$subject_replaces  = array(get_bloginfo( 'name' ));
			
			$subject = str_replace($subject_keywords, $subject_replaces, $adforest_theme['sb_new_rating_subject']);
			
			// Rator info
			$sender_info = get_userdata( $sender_id );

			$msg_keywords  = array('%site_name%', '%receiver%', '%rator%', '%rating%', '%comments%' , '%rating_link%' );
			$msg_replaces  = array(get_bloginfo( 'name' ), $receiver_info->display_name, $sender_info->display_name , $rating, $comments, get_author_posts_url( $receiver_id ).'?type=1' );
			
			$body = str_replace($msg_keywords, $msg_replaces, $adforest_theme['sb_new_rating_message']);

		}
		wp_mail( $to, $subject, $body, $headers );
}

function adforest_send_email_new_bid( $sender_id, $receiver_id, $bid = '', $comments = '', $aid )
{
		global $adforest_theme;
		$receiver_info = get_userdata($receiver_id);
		$to = $receiver_info->user_email;
		$from	=	'';
		if( isset( $adforest_theme['sb_new_bid_from'] ) && $adforest_theme['sb_new_bid_from'] != "" )
		{
			$from	=	$adforest_theme['sb_new_bid_from'];
		}
			$headers = array('Content-Type: text/html; charset=UTF-8',"From: $from" );
		if( isset( $adforest_theme['sb_new_bid_message'] ) &&  $adforest_theme['sb_new_bid_message'] != "" )
		{
			
			
			
			$subject_keywords  = array('%site_name%');
			$subject_replaces  = array(get_bloginfo( 'name' ));
			
			$subject = str_replace($subject_keywords, $subject_replaces, $adforest_theme['sb_new_bid_subject']);
			
			// Bidder info
			$sender_info = get_userdata( $sender_id );

			$msg_keywords  = array('%site_name%', '%receiver%', '%bidder%', '%bid%', '%comments%' , '%bid_link%' );
			$msg_replaces  = array(get_bloginfo( 'name' ), $receiver_info->display_name, $sender_info->display_name , $bid, $comments, get_the_permalink($aid).'#tab2default' );
			
			$body = str_replace($msg_keywords, $msg_replaces, $adforest_theme['sb_new_bid_message']);
			wp_mail( $to, $subject, $body, $headers );
		}
}

// Resend Email
add_action('wp_ajax_sb_resend_email', 'adforest_resend_email');
add_action( 'wp_ajax_nopriv_sb_resend_email', 'adforest_resend_email' );
function adforest_resend_email()
{
	$email	= $_POST['usr_email'];
	$user = get_user_by( 'email', $email );
	if( get_user_meta($user->ID, 'sb_resent_email', true) != 'yes' )
	{
		
		adforest_email_on_new_user($user->ID, '', false);
		update_user_meta($user->ID, 'sb_resent_email', 'yes');
	}
	die();
}

// Email on new User
function adforest_email_on_new_user($user_id, $social = '', $admin_email = true)
{
	global $adforest_theme;
	
	if( isset( $adforest_theme['sb_new_user_email_to_admin'] ) && $adforest_theme['sb_new_user_email_to_admin'] && $admin_email )
	{
		if( isset( $adforest_theme['sb_new_user_admin_message'] ) &&  $adforest_theme['sb_new_user_admin_message'] != "" && isset( $adforest_theme['sb_new_user_admin_message_from'] ) && $adforest_theme['sb_new_user_admin_message_from'] != "" )
		{
			$to	=	get_option( 'admin_email' );
			$subject = $adforest_theme['sb_new_user_admin_message_subject'];
			$from	=	$adforest_theme['sb_new_user_admin_message_from'];
			$headers = array('Content-Type: text/html; charset=UTF-8',"From: $from" );
			
			// User info
			$user_info = get_userdata( $user_id );
			
			
			$msg_keywords  = array('%site_name%', '%display_name%', '%email%');
			$msg_replaces  = array(get_bloginfo( 'name' ), $user_info->display_name, $user_info->user_email );
			
			
			$body = str_replace($msg_keywords, $msg_replaces, $adforest_theme['sb_new_user_admin_message']);
			wp_mail( $to, $subject, $body, $headers );
		}
		
	}
	
	if( isset( $adforest_theme['sb_new_user_email_to_user'] ) && $adforest_theme['sb_new_user_email_to_user'] )
	{
		if( isset( $adforest_theme['sb_new_user_message'] ) &&  $adforest_theme['sb_new_user_message'] != "" && isset( $adforest_theme['sb_new_user_message_from'] ) && $adforest_theme['sb_new_user_message_from'] != "" )
		{
			// User info
			$user_info = get_userdata( $user_id );
			
			$to	=	$user_info->user_email;
			$subject = $adforest_theme['sb_new_user_message_subject'];
			$from	=	$adforest_theme['sb_new_user_message_from'];
			$headers = array('Content-Type: text/html; charset=UTF-8',"From: $from" );
			

			
			$user_name	=	$user_info->user_email;
			if( $social != '' )
				$user_name .= "(Password: $social )";
			
			$verification_link	=	'';
			if( isset( $adforest_theme['sb_new_user_email_verification'] ) && $adforest_theme['sb_new_user_email_verification'] && $social == "" )
			{
				$token	=	get_user_meta($user_id, 'sb_email_verification_token', true);
				if( $token == "" )
				{
					$token 	=	 adforest_randomString(50);
				}
				$verification_link	=	trailingslashit( get_the_permalink( $adforest_theme['sb_sign_in_page'] ) ) . '?verification_key=' . $token . '-sb-uid-'. $user_id;

				update_user_meta($user_id, 'sb_email_verification_token', $token);
			}
			
			$msg_keywords  = array('%site_name%', '%user_name%', '%display_name%', '%verification_link%');
			$msg_replaces  = array(get_bloginfo( 'name' ), $user_name, $user_info->display_name, $verification_link );
			
			$body = str_replace($msg_keywords, $msg_replaces, $adforest_theme['sb_new_user_message']);
			wp_mail( $to, $subject, $body, $headers );
		}
	}

}

// Email on Ad approval
function adforest_get_notify_on_ad_approval($pid)
{
	global $adforest_theme;
	$from	=	get_bloginfo( 'name' );
	if( isset( $adforest_theme['sb_active_ad_email_from'] ) && $adforest_theme['sb_active_ad_email_from'] != "" )
	{
		$from	=	$adforest_theme['sb_active_ad_email_from'];
	}
	$headers = array('Content-Type: text/html; charset=UTF-8',"From: $from" );
	if( isset( $adforest_theme['sb_active_ad_email_message'] ) &&  $adforest_theme['sb_active_ad_email_message'] != "" )
	{
		
		$author_id = get_post_field ('post_author', $pid);
		$user_info = get_userdata($author_id);
		
		$subject = $adforest_theme['sb_active_ad_email_subject'];

		$msg_keywords  = array('%site_name%', '%user_name%', '%ad_title%', '%ad_link%');
		$msg_replaces  = array(get_bloginfo( 'name' ), $user_info->display_name, get_the_title($pid), get_the_permalink($pid) );
		
		$to = $user_info->user_email;
		$body = str_replace($msg_keywords, $msg_replaces, $adforest_theme['sb_active_ad_email_message']);
		wp_mail( $to, $subject, $body, $headers );
	}
}

// Email on Ad rating
function adforest_email_ad_rating($pid,$sender_id, $rating, $comments)
{
	global $adforest_theme;
	$from	=	get_bloginfo( 'name' );
	if( isset( $adforest_theme['ad_rating_email_from'] ) && $adforest_theme['ad_rating_email_from'] != "" )
	{
		$from	=	$adforest_theme['ad_rating_email_from'];
	}
	$headers = array('Content-Type: text/html; charset=UTF-8',"From: $from" );
	if( isset( $adforest_theme['ad_rating_email_message'] ) &&  $adforest_theme['ad_rating_email_message'] != "" )
	{
		
		$author_id = get_post_field ('post_author', $pid);
		$user_info = get_userdata($author_id);
		
		$subject = $adforest_theme['ad_rating_email_subject'];

		$msg_keywords  = array('%site_name%', '%ad_title%', '%ad_link%', '%rating%', '%rating_comments%', '%author_name%');
		$msg_replaces  = array(get_bloginfo( 'name' ), get_the_title($pid), get_the_permalink($pid) . '#ad-rating', $rating, $comments, $user_info->display_name );
		
		$to = $user_info->user_email;
		$body = str_replace($msg_keywords, $msg_replaces, $adforest_theme['ad_rating_email_message']);
		wp_mail( $to, $subject, $body, $headers );
	}
}

// Email on Ad rating reply
function adforest_email_ad_rating_reply($pid,$receiver_id, $reply, $rating, $rating_comments)
{
	global $adforest_theme;
	$from	=	get_bloginfo( 'name' );
	if( isset( $adforest_theme['ad_rating_reply_email_from'] ) && $adforest_theme['ad_rating_reply_email_from'] != "" )
	{
		$from	=	$adforest_theme['ad_rating_reply_email_from'];
	}
	$headers = array('Content-Type: text/html; charset=UTF-8',"From: $from" );
	if( isset( $adforest_theme['ad_rating_reply_email_message'] ) &&  $adforest_theme['ad_rating_reply_email_message'] != "" )
	{
		
		$author_id = get_post_field ('post_author', $pid);
		$user_info = get_userdata($author_id);
		
		$subject = $adforest_theme['ad_rating_reply_email_subject'];

		$msg_keywords  = array('%site_name%', '%ad_title%', '%ad_link%', '%rating%', '%rating_comments%', '%author_name%', '%author_reply%');
		$msg_replaces  = array(get_bloginfo( 'name' ), get_the_title($pid), get_the_permalink($pid) . '#ad-rating', $rating, $rating_comments, $user_info->display_name, $reply );
		
		$receiver_info = get_userdata($receiver_id);
		$to = $receiver_info->user_email;
		$body = str_replace($msg_keywords, $msg_replaces, $adforest_theme['ad_rating_reply_email_message']);
		wp_mail( $to, $subject, $body, $headers );
	}
}

// Ajax handler for add to cart
add_action( 'wp_ajax_demo_data_start', 'adforest_before_install_demo_data' );
// Addind Subcriber into Mailchimp
function adforest_before_install_demo_data()
{
	if( get_option( 'adforest_fresh_installation' ) != 'no' )
	{
		update_option( 'adforest_fresh_installation', $_POST['is_fresh'] );
	}
	die();
}

// Importing data
function adforest_importing_data($demo_type)
{
	global $wpdb;
	$sql_file_OR_content;
	if( $demo_type == 'Adforest' )
		$sql_file_OR_content	=	SB_PLUGIN_PATH . 'sql/data.sql';
	else
		$sql_file_OR_content	=	SB_PLUGIN_PATH . 'sql/data-rtl.sql';
		
	$SQL_CONTENT = (strlen($sql_file_OR_content) > 300 ?  $sql_file_OR_content : file_get_contents($sql_file_OR_content)  );  
	$allLines = explode("\n",$SQL_CONTENT); 
	$zzzzzz = $wpdb->query('SET foreign_key_checks = 0');
	preg_match_all("/\nCREATE TABLE(.*?)\`(.*?)\`/si", "\n". $SQL_CONTENT, $target_tables);
	foreach ($target_tables[2] as $table)
	{
		$wpdb->query('DROP TABLE IF EXISTS '.$table);
	}
	$zzzzzz = $wpdb->query('SET foreign_key_checks = 1');
	//$wpdb->query("SET NAMES 'utf8'");	
	$templine = '';	// Temporary variable, used to store current query
	foreach ($allLines as $line)
	{											// Loop through each line
		if (substr($line, 0, 2) != '--' && $line != '')
		{
			$templine .= $line; 	// (if it is not a comment..) Add this line to the current segment
			if (substr(trim($line), -1, 1) == ';')
			{		// If it has a semicolon at the end, it's the end of the query
				if( $wpdb->prefix != 'wp_' )
				{
					$templine = str_replace("`wp_", "`$wpdb->prefix", $templine);
				}
				if(!$wpdb->query($templine))
				{ 
					//print('Error performing query \'<strong>' . $templine . '\': ' . $wpdb->error . '<br /><br />');
				}  
				$templine = ''; // set variable to empty, to start picking up the lines after ";"
			}
		}
	}	
	//return 'Importing finished. Now, Delete the import file.';
}

// define the admin_comment_types_dropdown callback 
function sb_filter_admin_comment_types_dropdown( $comment_type_array ) { 
    // make filter magic happen here... 
	$comment_type_array['ad_post_rating']	=	__('Ad Rating', 'redux-framework' );
    return $comment_type_array; 
}; 
         
// add the filter 
add_filter( 'admin_comment_types_dropdown', 'sb_filter_admin_comment_types_dropdown', 10, 1 ); 

add_action( 'wp_ajax_sb_delete_user_rating', 'adforest_delete_user_rating' );
// Delete user rating
function adforest_delete_user_rating()
{
	global $wpdb;
	$meta_id	=	$_POST['meta_id'];
	$table_name	=	$wpdb->prefix . "usermeta";
	$wpdb->query( "DELETE FROM $table_name WHERE umeta_id = '$meta_id' " );
	echo "1";
	die();
}

add_action( 'wp_ajax_sb_delete_user_bid', 'adforest_delete_user_bid_admin' );
// Delete user rating
function adforest_delete_user_bid_admin()
{
	global $wpdb;
	$meta_id	=	$_POST['meta_id'];
	$table_name	=	$wpdb->prefix . "postmeta";
	$wpdb->query( "DELETE FROM $table_name WHERE meta_id = '$meta_id' " );
	echo "1";
	die();
}

// Email on new User
add_action('wp_ajax_sb_user_contact_form', 'adforest_user_contact_form');
add_action('wp_ajax_nopriv_sb_user_contact_form', 'adforest_user_contact_form');
function adforest_user_contact_form()
{
	global $adforest_theme;
	$params = array();
    parse_str($_POST['sb_data'], $params);
	$name	=	$params['name'];
	$email	=	$params['email'];
	$sender_subject	=	$params['subject'];
	$message	=	$params['message'];
	$user_id	=	$_POST['receiver_id'];
		
		if( isset($adforest_theme['user_contact_form'] ) && $adforest_theme['user_contact_form'] )
		{
		if( isset( $adforest_theme['sb_profile_contact_message'] ) &&  $adforest_theme['sb_profile_contact_message'] != "" && isset( $adforest_theme['sb_profile_contact_from'] ) && $adforest_theme['sb_profile_contact_from'] != "" )
		{
			$user_info = get_userdata( $user_id );
			$to	=	$user_info->user_email;
			$user_info->display_name;
			$subject = $adforest_theme['sb_profile_contact_subject'];
			$from	=	$adforest_theme['sb_profile_contact_from'];
			$headers = array('Content-Type: text/html; charset=UTF-8',"From: $from" );
			
			
			$msg_keywords  = array('%receiver_name%', '%sender_name%', '%sender_email%', '%sender_subject%', '%sender_message%');
			$msg_replaces  = array($user_info->display_name, $name, $email, $sender_subject, $message );
			
			
			$body = str_replace($msg_keywords, $msg_replaces, $adforest_theme['sb_profile_contact_message']);
			$res	=	wp_mail( $to, $subject, $body, $headers );
			if( $res )
			{
				 echo '1|' . __( "Message has been sent.", 'adforest' );
			}
			else
			{
				 echo '0|' . __( "Message not sent, please try later.", 'adforest' );
			
			}
			die();
		}
		}

}
