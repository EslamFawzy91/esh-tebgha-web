<?php

function adforest_taxonomy_add_new_meta_field($term){
 
	$term_id = $term->term_id;
	$html = '';
	$result = get_term_meta( $term_id , '_sb_dynamic_form_fields' , true);
		if(isset($result) && $result != "")
		{
			$formData = sb_dynamic_form_data($result);	
			foreach($formData as $r)
			{
				if(trim($r['types']) != "") { $html .= sb_dynamic_form_fields($r, 'yes'); }
			}
		}
	
			$templatePriceShow = '_sb_default_cat_price_show';
			$templatePriceRequired = '_sb_default_cat_price_required';
	
			$templatePriceShowValue = sb_custom_form_data($result, $templatePriceShow);
			$templatePriceRequiredValue = sb_custom_form_data($result, $templatePriceRequired);	

			$templatePriceTypeShow = '_sb_default_cat_price_type_show';
			$templatePriceTypeRequired = '_sb_default_cat_price_type_required';
	
			$templatePriceTypeShowValue = sb_custom_form_data($result, $templatePriceTypeShow);
			$templatePriceTypeRequiredValue = sb_custom_form_data($result, $templatePriceTypeRequired);	
	
			$templateVideoShow = '_sb_default_cat_video_show';
			$templateVideoRequired = '_sb_default_cat_video_required';

			$templateVideoShowValue = sb_custom_form_data($result, $templateVideoShow);
			$templateVideoRequiredValue = sb_custom_form_data($result, $templateVideoRequired);	

	
			$templateTagsShow = '_sb_default_cat_tags_show';
			$templateTagsRequired = '_sb_default_cat_tags_required';

			$templateTagsShowValue = sb_custom_form_data($result, $templateTagsShow);
			$templateTagsRequiredValue = sb_custom_form_data($result, $templateTagsRequired);	

	
			$templateImageShow = '_sb_default_cat_image_show';
			$templateImageRequired = '_sb_default_cat_image_required';
	
			$templateImageShowValue = sb_custom_form_data($result, $templateImageShow);
			$templateImageRequiredValue = sb_custom_form_data($result, $templateImageRequired);	
	

			$templateConditionShow = '_sb_default_cat_condition_show';
			$templateConditionRequired = '_sb_default_cat_condition_required';
	
			$templateConditionShowValue = sb_custom_form_data($result, $templateConditionShow);
			$templateConditionRequiredValue = sb_custom_form_data($result, $templateConditionRequired);	
	

			$templateWarrantyShow = '_sb_default_cat_warranty_show';
			$templateWarrantyRequired = '_sb_default_cat_warranty_required';
	
			$templateWarrantyShowValue = sb_custom_form_data($result, $templateWarrantyShow);
			$templateWarrantyRequiredValue = sb_custom_form_data($result, $templateWarrantyRequired);	
	
			//$templateAdTypeShow, $templateAdTypeShowValue, $templateAdTypeRequired, $templateAdTypeRequiredValue

			$templateAdTypeShow = '_sb_default_cat_ad_type_show';
			$templateAdTypeRequired = '_sb_default_cat_ad_type_required';

			$templateAdTypeShowValue = sb_custom_form_data($result, $templateAdTypeShow);
			$templateAdTypeRequiredValue = sb_custom_form_data($result, $templateAdTypeRequired);		
	
	?>
	                   			              	     			              
	     			                   			              
<table class="wp-list-table widefat striped">
	
<tbody>
	<tr>
	<td colspan="3">
		
		<p class="submit inline-edit-save">
				<strong><?php //echo __('Those are default fields you can show or hide them.', 'redux-framework'); ?></strong>
				</p>
		
	</td> </tr>
	<tr class="user-rich-editing-wrap">
	<th class="name column-name">
		<strong><?php echo __('Field Name', 'redux-framework'); ?></strong>
	</th>					
	<th class="username column-username has-row-actions column-primary">
	<strong><?php echo __('Status', 'redux-framework'); ?></strong>			
	</th>
	<th class="username column-username has-row-actions column-primary">
		<strong><?php echo __('Is Required?', 'redux-framework'); ?></strong>
	</th>
  </tr>
	<tr class="user-rich-editing-wrap">
	<td class="name column-name">
							

		<label for="rich_editing"><?php echo __('Price', 'redux-framework'); ?>:</label>
	</td>					
	<td class="username column-username has-row-actions column-primary">
		<label for="<?php echo $templatePriceShow; ?>">

		<select name="<?php echo $templatePriceShow; ?>" id="<?php echo $templatePriceShow; ?>">
			<option value="1" <?php echo adforest_option_selected($templatePriceShowValue, '1'); ?>><?php echo __('Show', 'redux-framework');?></option>
			<option value="0" <?php echo adforest_option_selected($templatePriceShowValue, '0'); ?>><?php echo __('Hide', 'redux-framework');?></option>
		</select>	
		</label>					
	</td>
	<td class="username column-username has-row-actions column-primary">
		<label for="<?php echo $templatePriceRequired; ?>">
		
		<select name="<?php echo $templatePriceRequired; ?>" id="<?php echo $templatePriceRequired; ?>">
				<option value="1" <?php echo adforest_option_selected($templatePriceRequiredValue, '1'); ?>><?php echo __('Yes', 'redux-framework');?></option>
				<option value="0" <?php echo adforest_option_selected($templatePriceRequiredValue, '0'); ?>><?php echo __('No', 'redux-framework');?></option>
		</select>	

	</td>
  </tr>
	<tr class="user-rich-editing-wrap">
	<td class="name column-name">
							

		<label for="rich_editing"><?php echo __('Price Type', 'redux-framework'); ?>:</label>
	</td>					
	<td class="username column-username has-row-actions column-primary">
		<label for="<?php echo $templatePriceTypeShow; ?>">

		<select name="<?php echo $templatePriceTypeShow; ?>" id="<?php echo $templatePriceTypeShow; ?>">
			<option value="1" <?php echo adforest_option_selected($templatePriceTypeShowValue, '1'); ?>><?php echo __('Show', 'redux-framework');?></option>
			<option value="0" <?php echo adforest_option_selected($templatePriceTypeShowValue, '0'); ?>><?php echo __('Hide', 'redux-framework');?></option>
		</select>	
		</label>					
	</td>
	<td class="username column-username has-row-actions column-primary">
		<label for="<?php echo $templatePriceTypeRequired; ?>">
		
		<select name="<?php echo $templatePriceTypeRequired; ?>" id="<?php echo $templatePriceTypeRequired; ?>">
				<option value="1" <?php echo adforest_option_selected($templatePriceTypeRequiredValue, '1'); ?>><?php echo __('Yes', 'redux-framework');?></option>
				<option value="0" <?php echo adforest_option_selected($templatePriceTypeRequiredValue, '0'); ?>><?php echo __('No', 'redux-framework');?></option>
		</select>	

	</td>
  </tr>

	<tr class="user-rich-editing-wrap">
	<td class="name column-name">
		<label for="rich_editing"><?php echo __('Video URL', 'redux-framework');?></label>
	</td>					
	<td class="username column-username has-row-actions column-primary">
		<label for="<?php echo $templateVideoShow; ?>">

		<select name="<?php echo $templateVideoShow; ?>" id="<?php echo $templateVideoShow; ?>">
			<option value="1"  <?php echo adforest_option_selected($templateVideoShowValue, '1'); ?>><?php echo __('Show', 'redux-framework');?></option>
			<option value="0" <?php echo adforest_option_selected($templateVideoShowValue, '0'); ?>><?php echo __('Hide', 'redux-framework');?></option>

		</select>	
		</label>					
	</td>
	<td class="username column-username has-row-actions column-primary">
		<label for="<?php echo $templateVideoRequired; ?>">
		
		<select name="<?php echo $templateVideoRequired; ?>" id="<?php echo $templateVideoRequired; ?>">
				<option value="1" <?php echo adforest_option_selected($templateVideoRequiredValue, '1'); ?>><?php echo __('Yes', 'redux-framework');?></option>
				<option value="0" <?php echo adforest_option_selected($templateVideoRequiredValue, '0'); ?>><?php echo __('No', 'redux-framework');?></option>
		</select>			
		</label>
	</td>
  </tr>

	<tr class="user-rich-editing-wrap">
	<td class="name column-name">
		<label for="rich_editing"><?php echo __('Ad Tags', 'redux-framework');?></label>
	</td>					
	<td class="username column-username has-row-actions column-primary">
		<label for="<?php echo $templateTagsShow; ?>">

		<select name="<?php echo $templateTagsShow; ?>" id="<?php echo $templateTagsShow; ?>">
			<option value="1" <?php echo adforest_option_selected($templateTagsShowValue, '1'); ?>><?php echo __('Show', 'redux-framework');?></option>
			<option value="0" <?php echo adforest_option_selected($templateTagsShowValue, '0'); ?>><?php echo __('Hide', 'redux-framework');?></option>

		</select>	
		</label>					
	</td>
	<td class="username column-username has-row-actions column-primary">
		<label for="<?php echo $templateTagsRequired; ?>">
		
		
		<select name="<?php echo $templateTagsRequired; ?>" id="<?php echo $templateTagsRequired; ?>">
				<option value="1"  <?php echo adforest_option_selected($templateTagsRequiredValue, '1'); ?>><?php echo __('Yes', 'redux-framework');?></option>
				<option value="0"  <?php echo adforest_option_selected($templateTagsRequiredValue, '0'); ?>><?php echo __('No', 'redux-framework');?></option>
		</select>		
	</label>
	</td>
  </tr>
	<tr class="user-rich-editing-wrap">
	<td class="name column-name">
		<label for="rich_editing"><?php echo __('Ad Images', 'redux-framework'); ?></label>
	</td>					
	<td class="username column-username has-row-actions column-primary">
		<label for="<?php echo $templateImageShow; ?>">

		<select name="<?php echo $templateImageShow; ?>" id="<?php echo $templateImageShow; ?>">
			<option value="1" <?php echo adforest_option_selected($templateImageShowValue, '1'); ?>><?php echo __('Show', 'redux-framework');?></option>
			<option value="0" <?php echo adforest_option_selected($templateImageShowValue, '0'); ?>><?php echo __('Hide', 'redux-framework');?></option>
		</select>	
		</label>					
	</td>
	<td class="username column-username has-row-actions column-primary">
		<label for="<?php echo $templateImageRequired; ?>">
		
		<select name="<?php echo $templateImageRequired; ?>" id="<?php echo $templateImageRequired; ?>">
				<option value="1"  <?php echo adforest_option_selected($templateImageRequiredValue, '1'); ?>><?php echo __('Yes', 'redux-framework');?></option>
				<option value="0" <?php echo adforest_option_selected($templateImageRequiredValue, '0'); ?>><?php echo __('No', 'redux-framework');?></option>
		</select>			
	</td>
  </tr>
	<tr class="user-rich-editing-wrap">
	<td class="name column-name">
		<label for="rich_editing"><?php echo __('Item Condition', 'redux-framework'); ?></label>
	</td>					
	<td class="username column-username has-row-actions column-primary">
		<label for="<?php echo $templateConditionShow; ?>">

		<select name="<?php echo $templateConditionShow; ?>" id="<?php echo $templateConditionShow; ?>">
			<option value="1" <?php echo adforest_option_selected($templateConditionShowValue, '1'); ?>><?php echo __('Show', 'redux-framework');?></option>
			<option value="0" <?php echo adforest_option_selected($templateConditionShowValue, '0'); ?>><?php echo __('Hide', 'redux-framework');?></option>
		</select>	
		</label>					
	</td>
	<td class="username column-username has-row-actions column-primary">
		<label for="<?php echo $templateConditionRequired; ?>">
		
		
		<select name="<?php echo $templateConditionRequired; ?>" id="<?php echo $templateConditionRequired; ?>">
				<option value="1" <?php echo adforest_option_selected($templateConditionRequiredValue, '1'); ?>><?php echo __('Yes', 'redux-framework');?></option>
				<option value="0"  <?php echo adforest_option_selected($templateConditionRequiredValue, '0'); ?>><?php echo __('No', 'redux-framework');?></option>
		</select>	
	</label>
	</td>
  </tr>
	<tr class="user-rich-editing-wrap">
	<td class="name column-name">
		<label for="rich_editing"><?php echo __('Warranty', 'redux-framework'); ?></label>
	</td>					
	<td class="username column-username has-row-actions column-primary">
		<label for="<?php echo $templateWarrantyShow; ?>">

		<select name="<?php echo $templateWarrantyShow; ?>" id="<?php echo $templateWarrantyShow; ?>">
			<option value="1"  <?php echo adforest_option_selected($templateWarrantyShowValue, '1'); ?>><?php echo __('Show', 'redux-framework');?></option>
			<option value="0"  <?php echo adforest_option_selected($templateWarrantyShowValue, '0'); ?>><?php echo __('Hide', 'redux-framework');?></option>
		</select>	
		</label>					
	</td>
	<td class="username column-username has-row-actions column-primary">
			<label for="<?php echo $templateWarrantyRequired; ?>">
			<select name="<?php echo $templateWarrantyRequired; ?>" id="<?php echo $templateWarrantyRequired; ?>">
				<option value="1"  <?php echo adforest_option_selected($templateWarrantyRequiredValue, '1'); ?>><?php echo __('Yes', 'redux-framework');?></option>
				<option value="0"  <?php echo adforest_option_selected($templateWarrantyRequiredValue, '0'); ?>><?php echo __('No', 'redux-framework');?></option>
			</select>
		</label>
	</td>
  </tr>	

<tr class="user-rich-editing-wrap">
	<td class="name column-name">
		<label for="rich_editing"><?php echo __('Ad Type', 'redux-framework'); ?></label>
	</td>					
	<td class="username column-username has-row-actions column-primary">
		<label for="<?php echo $templateAdTypeShow; ?>">

		<select name="<?php echo $templateAdTypeShow; ?>" id="<?php echo $templateAdTypeShow; ?>">
			<option value="1" <?php echo adforest_option_selected($templateAdTypeShowValue, '1'); ?>><?php echo __('Show', 'redux-framework');?></option>
			<option value="0" <?php echo adforest_option_selected($templateAdTypeShowValue, '0'); ?>><?php echo __('Hide', 'redux-framework');?></option>
		</select>	
		</label>					
	</td>
	<td class="username column-username has-row-actions column-primary">
		<label for="<?php echo $templateAdTypeRequired; ?>">
		
		
		<select name="<?php echo $templateAdTypeRequired; ?>" id="<?php echo $templateAdTypeRequired; ?>">
				<option value="1" <?php echo adforest_option_selected($templateAdTypeRequiredValue, '1'); ?>><?php echo __('Yes', 'redux-framework');?></option>
				<option value="0"  <?php echo adforest_option_selected($templateAdTypeRequiredValue, '0'); ?>><?php echo __('No', 'redux-framework');?></option>
		</select>	
	</label>
	</td>
  </tr> 			    
  			  
  			  			  
  			  			  			  
  			  			  			  			  			  
</tbody>
</table>	     

             			                   			              






<!--ui-sortable-->
<div class="wrap-custom">
    <div id="poststuff">
        <div id="postbox-container" class="postbox-container">
            <div class="meta-box-sortables " id="normal-sortables">

	
	<table class="wp-list-table widefat striped">
		<tbody class="custom_fields_wrap custom_fields_table" id="sortable" >
			
			<?php echo $html; ?>
			<!--tr goes here-->		
		</tbody>
		<tfoot>
			<br />
			<tr>
				<td colspan="4">
					<input id="add-custom-field-button" class="button button-primary add_field_button" value="<?php echo __('Add More Fields', 'redux-framework');?>" type="button"> </label>
				</td>
			</tr>
		</tfoot>
	</table>    
  
              </div>
        </div>
    </div>
</div>

  
  
    <?php
    }

function adforest_option_selected($key, $val)
{
	return ($key == $val) ? 'selected="selected"' : '';
	
}
function sb_custom_form_data($result = '', $key = '')
{
	
	$arr = '';
	$res = array();
	if( $result != "" && $key != "")
	{
		$baseDecode = base64_decode($result);
		$arr =  json_decode($baseDecode, true);	
		$arr = is_array($arr) ? array_map('stripslashes_deep', $arr) : stripslashes($arr);
		if( isset( $arr["$key"] ) )
		{
			return ($arr["$key"]);
		}
		else
		{
			return 1;
		}
	}
	
}
  

function sb_dynamic_form_data($result = '')
{
	$arr = '';
	$res = array();
	if( $result != "")
	{
		$baseDecode = base64_decode($result);
		$arr =  json_decode($baseDecode, true);		
		
		$arr = is_array($arr) ? array_map('stripslashes_deep', $arr) : stripslashes($arr);
		
		$formTypes = isset($arr['_sb_dynamic_form_types']) ? $arr['_sb_dynamic_form_types'] : array();
		
		
		
		$countArr = count($formTypes);
		$i = 0;
		
		if($countArr > 0 && $formTypes != "")
		{
			for($i =0; $i <= $countArr; $i++)
			{
				$res[$i]['types']    = isset($arr['_sb_dynamic_form_types'][$i]) ? $arr['_sb_dynamic_form_types'][$i] : '';
				$res[$i]['titles']   = isset($arr['_sb_dynamic_form_titles'][$i]) ? $arr['_sb_dynamic_form_titles'][$i] : '';	
				$res[$i]['columns']   = isset($arr['_sb_dynamic_form_columns'][$i]) ? $arr['_sb_dynamic_form_columns'][$i] : '';
				$res[$i]['slugs'] 	 = isset($arr['_sb_dynamic_form_slugs'][$i]) ? $arr['_sb_dynamic_form_slugs'][$i] : '';	
				$res[$i]['values'] 	 = isset($arr['_sb_dynamic_form_values'][$i]) ? $arr['_sb_dynamic_form_values'][$i] : '';
				$res[$i]['status'] 	 = isset($arr['_sb_dynamic_form_status'][$i]) ? $arr['_sb_dynamic_form_status'][$i] : '';	
				$res[$i]['requires'] = isset($arr['_sb_dynamic_form_requires'][$i]) ? $arr['_sb_dynamic_form_requires'][$i] : '';
				$res[$i]['in_search'] = isset($arr['_sb_dynamic_form_in_search'][$i]) ? $arr['_sb_dynamic_form_in_search'][$i] : '';
				
			}
		}	
	}

	return $res;
}

function adforest_getCats_desc($postId)
{
 $terms = wp_get_post_terms( $postId, 'ad_cats', array( 'orderby' => 'id', 'order' => 'DESC' ) );
 $deepestTerm = false;
 $maxDepth = -1;
 $c = 0;
 foreach ($terms as $term) 
 {
  $ancestors = get_ancestors( $term->term_id, 'ad_cats' );
  $termDepth = count($ancestors);
   $deepestTerm[$c] = $term;
   $maxDepth = $termDepth;
   $c++;
 } 
 return ($deepestTerm);
}
function adforestCustomFieldsHTML($post_id = '', $cols = 4)
{
	global $adforest_theme;
	if($post_id == "") return;
	$html = '';
    $terms = adforest_getCats_desc( $post_id );
	if(isset( $terms ) && count( (array) $terms) > 0 && $terms != "" ){
     foreach ($terms as $term) {
		 if( isset( $term->term_id ) && $term->term_id != "" )
		 {
			$term_id = $term->term_id;
			$t = adforest_dynamic_templateID($term_id);
			if($t) break;
		 }
     }	
	
	//$termTemplate = get_term_meta( $term_id , '_sb_category_template' , true);
	//$result = get_term_meta( $termTemplate , '_sb_dynamic_form_fields' , true);

	$templateID = adforest_dynamic_templateID($term_id);
	$result = get_term_meta( $templateID , '_sb_dynamic_form_fields' , true);	
		
		
	
	if(isset($result) && $result != "")
	{
		$formData = sb_dynamic_form_data($result);	
		if(count($formData) > 0)
		{
			foreach($formData as $data)
			{	
				if($data['titles'] != "")
				{
					$values   = get_post_meta($post_id, "_adforest_tpl_field_".$data['slugs'], true );
					$value = json_decode($values);
					$value = (is_array( $value ) ) ? implode($value, ", ") : $values;
					$titles   = ($data['titles']);					
					if( $value != "" )
					{
						if(isset( $data['types'] ) && $data['types'] == 5)
						{
							$value = '<a href="'.esc_url($value).'" target="_blank">'.$adforest_theme['sb_link_text'].'</a>';
						}
						else if(isset( $data['types'] ) && $data['types'] == 4)
						{
							$value = date_i18n( get_option( 'date_format' ), strtotime( $value ) );
						}						
						else
						{
						    $value = esc_html($value);	
						}
						
						$html .=  '<div class="col-sm-'.$cols.' col-md-'.$cols.' col-xs-12 no-padding">
						<span><strong>'.esc_html($titles).'</strong> :</span>
						'.($value).'
						</div>';
					}
				}
			}
		}
	}
	}
	return $html;
	
}




function sb_dynamic_form_fields($results = '', $loop = '')
{
	$type = $title = $value = $status = $require = $selectVals = $columns = $slugs = $in_search = '' ;
	if($loop != "" && $results != "")
	{
		$type  		= (isset($results['types'])) ? $results['types'] : '';
		$title 		= (isset($results['titles'])) ? $results['titles'] : '';
		$slugs    	= (isset($results['slugs'])) ? $results['slugs'] : '';
		$columns   	= (isset($results['columns'])) ? $results['columns'] : '';
		$value 		= (isset($results['values'])) ? $results['values'] : '';
		$status 	= (isset($results['status'])) ? $results['status'] : '';
		$require  	= (isset($results['requires'])) ? $results['requires'] : '';
		
		$in_search 	= (isset($results['in_search'])) ? $results['in_search'] : '';
		
	}
	
	/*Get values and add in fields starts*/
		
		$type1 = ($type == 1) ? 'selected="selected"' : '';
	    $type2 = ($type == 2) ? 'selected="selected"' : '';
	    $type3 = ($type == 3) ? 'selected="selected"' : '';
		$type4 = ($type == 4) ? 'selected="selected"' : '';
		$type5 = ($type == 5) ? 'selected="selected"' : '';
	
		$textareaHide = ($type == 2 || $type == 3) ? '' : 'style="display:none;"';

		$status1 = ($status == 1) ? 'selected="selected"' : '';
	    $status2 = ($status == 0) ? 'selected="selected"' : '';

		$require1 = ($require == 1) ? 'selected="selected"' : '';
	    $require2 = ($require == 0) ? 'selected="selected"' : '';
	
		$Columnselected1 = ($columns == 12) ? 'selected="selected"' : '';
		$Columnselected2 = ($columns == 6) ? 'selected="selected"' : '';
		$Columnselected3 = ($columns == 4) ? 'selected="selected"' : '';
		$Columnselected4 = ($columns == 3) ? 'selected="selected"' : '';
	
		$inSearchSelect1 = ($in_search == 'no') ? 'selected="selected"' : '';
		$inSearchSelect2 = ($in_search == 'yes') ? 'selected="selected"' : '';
	/*Get values and add in fields ends */

	$selectNameAttr 	= '_sb_dynamic_form_types[]';
	$inputNameAttr		= '_sb_dynamic_form_titles[]';
	$columnSelect		= '_sb_dynamic_form_columns[]';
	$inputSlugNameAttr	= '_sb_dynamic_form_slugs[]';
	$checkboxNameAttr 	= '_sb_dynamic_form_requires[]';
	$remBtnAttr 		= '_sb_dynamic_form_removes[]';
	$statusBtnAttr 		= '_sb_dynamic_form_status[]';
	$valuesAttr 		= '_sb_dynamic_form_values[]';
	$inSearchSelect 	= '_sb_dynamic_form_in_search[]';

	$fieldName = __('Field Name', 'redux-framework');
	$fieldSlugName = __('Slug Name', 'redux-framework');
	$columnName = __('Columns', 'redux-framework');
	$slectName = __('Select Option', 'redux-framework');
	$valuesName = __('Enter Values', 'redux-framework');
	$requiredName = __('Required?', 'redux-framework');
	$remdName = __('Remove Field', 'redux-framework');
	$statusName = __('Status', 'redux-framework');	
	$inSearchName = __('In Search', 'redux-framework');	
	
	$dynamic = '';//__('Those are dynamic fields you can add texfield and select options as much as you want.', 'redux-framework');
	
	$titleName = isset($title) ? $title : __('New Field', 'redux-framework');
	$slugsId  = (isset($slugs) && $slugs != "") ? $slugs : rand(1,100000);
	
	$moreHTML = '<tr class="inline-edit-row">
	<td>
	
<div class="postbox " id="postbox-1-'.$slugsId.'">
	<div title="'.__('Click to toggle', 'redux-framework').'" class="handlediv"><br></div>

<button type="button" class="handlediv button-link" aria-expanded="false">
<span class="toggle-indicator" aria-hidden="true"></span></button>
	
	<h3 class="hndle"><span>'.$titleName.'&nbsp;</span></h3>
	<div class="inside">
					
	
	<p class="submit inline-edit-save">
	<strong>'.$dynamic.'</strong>
	</p>
	<fieldset class="inline-edit-col-left">
		<br class="clear">
		<div>
			<div class="wp-clearfix">
				<label class="input-text-wrap">
				<span class="title">'.$slectName.'</span> 
				<select name="'.$selectNameAttr.'" class="hideValuesBox" required="required">
					<option value="">'. __('Select Option', 'redux-framework').'</option>
					<option value="1" '.$type1.'>'. __('Input - Textfield', 'redux-framework').'</option>
					<option value="2" '.$type2.'>'. __('Options - Select Box', 'redux-framework').'</option>						
					<option value="3" '.$type3.'>'. __('Options - Check Box', 'redux-framework').'</option>
					<option value="4" '.$type4.'>'. __('Date - Input', 'redux-framework').'</option>
					<option value="5" '.$type5.'>'. __('Website URL', 'redux-framework').'</option>															
				</select>
				
					&nbsp;&nbsp;&nbsp;
			<label class="inline-edit-status alignright">
			<span class="title">'. $inSearchName .'</span>
			<select name="'.$inSearchSelect.'" required="required">
				<option value="yes" '.$inSearchSelect2.'>'. __('Yes', 'redux-framework').'</option>
				<option value="no" '.$inSearchSelect1.'>'. __('No', 'redux-framework').'</option>
				
			</select>
			
		</label>	

				&nbsp;&nbsp;&nbsp;
			<label class="inline-edit-status alignright">
			<span class="title">'. $columnName .'</span>
			<select name="'.$columnSelect.'" required="required">
				<option value="12" '.$Columnselected1.'>'. __('1/12 Column', 'redux-framework').'</option>
				<option value="6" '.$Columnselected2.'>'. __('1/6 Column', 'redux-framework').'</option>
				<option value="4" '.$Columnselected3.'>'. __('1/3 Column', 'redux-framework').'</option>
				<option value="3" '.$Columnselected4.'>'. __('1/4 Column', 'redux-framework').'</option>
			</select>
			
		</label>

				&nbsp;&nbsp;&nbsp;
			<label class="inline-edit-status alignright">
			<span class="title">'. $requiredName .'</span>
			<select name="'.$checkboxNameAttr.'" required="required">
				<option value="">'. __('Select Option', 'redux-framework').'</option>
				<option value="1" '.$require1.'>'. __('Yes', 'redux-framework').'</option>
				<option value="0" '.$require2.'>'. __('No', 'redux-framework').'</option>						
			</select>
			
		</label>				
				&nbsp;&nbsp;&nbsp;
			<label class="inline-edit-status alignright">
			<span class="title">'. $statusName .'</span>
			<select name="'.$statusBtnAttr.'" required="required">
				<option value="">'. __('Select Option', 'redux-framework').'</option>
				<option value="1" '.$status1.'>'. __('Active', 'redux-framework').'</option>
				<option value="0" '.$status2.'>'. __('Inactive', 'redux-framework').'</option>						
			</select>
			&nbsp;&nbsp;&nbsp;
			</label>			
			</div>
			<div class="wp-clearfix">
			<label>
			<span class="title">'.$fieldName.'</span>
			<span class="input-text-wrap">
				<input class="ptitle sb-get-tilte" value="'.$title.'" type="text" name="'.$inputNameAttr.'" required="required">
				<span>'.__("Enter Field title here.", "redux-framework").'</span>
			</span> 
		</label>
		<label>
		<span class="title">'.$fieldSlugName.'</span>
		<span class="input-text-wrap">
			<input class="ptitle sb-get-slug" value="'.$slugs.'" type="text" name="'.$inputSlugNameAttr.'" required="required" readonly>
			<input type="Checkbox" class="sb-get-slug-edit"><strong>('.__("Edit","redux-framework").')</strong>
			<span>'.__("Enter the slug name, it must be unique and chage it with causion. only alpha numeric and _", "redux-framework").'</span>
		</span> 
	</label>	
	</div>
		<label class="values-label" '.$textareaHide.'>
			<span class="title">'. $valuesName .'</span>
			<span class="input-text-wrap">
			<textarea cols="22" rows="1" class="tax_input_post_tag" name="'.$valuesAttr.'">'.$value.'</textarea>
				<span>'.__("Enter Values seprated by ", "redux-framework").' | </span>
			</span>
		</label>
<button type="button" class="button button-primary cancel alignright sb_custom_rem_btn"  name="'.$remBtnAttr.'">' .$remdName. '</button>
<br class="clear">
<br class="clear">
	</div>
	</fieldset>
	<br class="clear">	
	

                    </div>
                </div>	
	</td>
	</tr>';
	
	
	return ' '.$moreHTML.'';
}

// Save extra taxonomy fields callback function.
function my_taxonomy_save_taxonomy_meta( $term_id ) {
	
	if ( isset( $_POST ) ) {
		
	
		$data = wp_json_encode( $_POST);
		$data = base64_encode($data);
		
		update_term_meta( $term_id, '_sb_dynamic_form_fields', $data );		
	}
}  


//add_action( 'created_sb_dynamic_form_templates', 'my_taxonomy_save_taxonomy_meta');
add_action( 'edit_sb_dynamic_form_templates', 'my_taxonomy_save_taxonomy_meta');

// Register Custom Taxonomy
function custom_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Form Templates', 'Taxonomy General Name', 'redux-framework'),
		'singular_name'              => _x( 'Form Template', 'Taxonomy Singular Name', 'redux-framework'),
		'menu_name'                  => __( 'Category Templates', 'redux-framework'),
		'all_items'                  => __( 'All Items', 'redux-framework'),
		'parent_item'                => __( 'Parent Item', 'redux-framework'),
		'parent_item_colon'          => __( 'Parent Item:', 'redux-framework'),
		'new_item_name'              => __( 'New Item Name', 'redux-framework'),
		'add_new_item'               => __( 'Add New Item', 'redux-framework'),
		'edit_item'                  => __( 'Edit Item', 'redux-framework'),
		'update_item'                => __( 'Update Item', 'redux-framework'),
		'view_item'                  => __( 'View Item', 'redux-framework'),
		'separate_items_with_commas' => __( 'Separate items with commas', 'redux-framework'),
		'add_or_remove_items'        => __( 'Add or remove items', 'redux-framework'),
		'choose_from_most_used'      => __( 'Choose from the most used', 'redux-framework'),
		'popular_items'              => __( 'Popular Items', 'redux-framework'),
		'search_items'               => __( 'Search Items', 'redux-framework'),
		'not_found'                  => __( 'Not Found', 'redux-framework'),
		'no_terms'                   => __( 'No items', 'redux-framework'),
		'items_list'                 => __( 'Items list', 'redux-framework'),
		'items_list_navigation'      => __( 'Items list navigation', 'redux-framework'),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => false,
		'show_ui'                    => true,
		'show_admin_column'          => false,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'sb_dynamic_form_templates', array( 'ad_post' ), $args );

}
add_action( 'init', 'custom_taxonomy', 0 );

add_action( 'sb_dynamic_form_templates_edit_form_fields', 'adforest_taxonomy_add_new_meta_field', 10, 2 );

/* Add Javascript/Jquery Code Here */
if( isset($_GET['taxonomy'] ) && 'sb_dynamic_form_templates' == $_GET['taxonomy'])
{
	add_action('admin_footer','adforest_admin_scripts_enqueue_cat_templates');	
}
function adforest_admin_scripts_enqueue_cat_templates()
{
	wp_enqueue_script( 'postbox' );
	$confirmDelMeg = __('Are You sure you want to remove this field.', 'redux-framework');
	$moreHTML = sb_dynamic_form_fields();
	$output = str_replace(array("\r\n", "\r"), "\n", $moreHTML);
	$lines = explode("\n", $output);
	$new_lines = array();
	foreach ($lines as $i => $line) {
		if(!empty($line))
			$new_lines[] = trim($line);
	}
	$moreHTML = implode($new_lines);
	?>
                       
<style type="text/css">
  #sortable { list-style-type: none; margin: 0; padding: 0; width: 60%; }
  #sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; height: 18px; }
  #sortable li span { position: absolute; margin-left: -1.3em; }
	.custom_fields_table tr{
		border: 1px solid #CCCACA;
	}
	
	#sortable .postbox
	{
		margin-bottom: 10px !important; 
		margin-top: 10px !important;
	}	
  </style>
 
 
  <script type="text/javascript">
  jQuery( function() {
    jQuery( "#sortable" ).sortable();
    jQuery( "#sortable" ).disableSelection();
  } );
	  
jQuery(document).on('ready', function($){
    postboxes.save_state = function(){
        return;
    };
    postboxes.save_order = function(){
        return;
    };
    postboxes.add_postbox_toggles();
});	  
  </script>
<script type="text/javascript">
		

function sb_confirm_delete_template(){confirm('<?php echo $confirmDelMeg; ?>'); return; }

jQuery(document).ready(function() {

	var max_fields      = 10000; 
    var wrapper         = jQuery(".custom_fields_wrap"); 
    var add_button      = jQuery("#add-custom-field-button");
	var rmv_add_button  = jQuery(".sb_custom_rem_btn"); 
   
    var x = 1; 
    jQuery(add_button).click(function(e){ 
        e.preventDefault();
        if(x < max_fields){ 
            x++; 
            jQuery(wrapper).append('<?php echo ($moreHTML); ?>'); 			
        }
    });
   
	jQuery(wrapper).on("click",".sb_custom_rem_btn", function(e){ 
        e.preventDefault(); 
		jQuery(this).closest('tr').remove(); x--;
    })
	
	
	jQuery(wrapper).on("click","#submit", function(e){ 
			jQuery('input.sb-get-slug').each(function() {
				if(jQuery(this).val() == this.defaultValue) {
						alert('<?php echo __("Duplicate Slug Name", "redux-framework"); ?>');
						return false;
				}		
			});
	});
	//sb-get-slug-edit
	jQuery(wrapper).on("click",".sb-get-slug-edit", function(e){ 
			var checkedval = jQuery(this).is(':checked');
			if(checkedval)
			{
				
				jQuery(this).closest('tr').find('input.sb-get-slug').removeAttr('readonly');
				
			}
			else
			{
				jQuery(this).closest('tr').find('input.sb-get-slug').attr('readonly','readonly');
			}
			
	 });
	
	jQuery(wrapper).on("change",".hideValuesBox", function(e){ 
	
        e.preventDefault(); 
		
		var selectVal = jQuery(this).val();
		if( selectVal == 1 || selectVal == 4 || selectVal == 5 )
		{
			jQuery(this).parent().parent().parent().find('label[class=values-label]').hide();
		}
		else
		{
			jQuery(this).parent().parent().parent().find('label[class=values-label]').show();
		}
				
		
	 });
	
	jQuery(wrapper).on("change",".sb-get-tilte", function(e){ 
		
		var isData = jQuery(this).parent().parent().parent().find('input.sb-get-slug').val();
		
		if(isData == "")
		{
			var selectVal 	= jQuery(this).val();
			var string 		= selectVal.toLowerCase();
			var slugVal 	=  string.trim().replace(/[^a-z0-9]+/gi, '_');		
			jQuery(this).parent().parent().parent().find('input.sb-get-slug').val(slugVal);
		}
	 });
	
});		


jQuery(document).ready(function($) 
{
     jQuery('.meta-box-sortables').sortable({
         opacity: 0.6,
         revert: true,
         cursor: 'move',
         handle: '.hndle'
     });
});	
	
	</script>
<?php
}



function sb_add_template_to_cat()
{
	
	$terms = get_terms( array( 'taxonomy' => 'sb_dynamic_form_templates', 'hide_empty' => false, 'parent' => 0, ) );

	$optionsHtml = '';
	$selected1	=	'';
	foreach($terms as $tr)
	{		
		$term_id = isset($_GET['tag_ID']) ? $_GET['tag_ID'] : '';
		$result = get_term_meta( $term_id , '_sb_category_template' , true);

		$selected = ($result == $tr->term_id) ? 'selected="selected"' : '';
		
		$optionsHtml  .= '<option value="'.esc_attr($tr->term_id).'" '.$selected.'>'.esc_html($tr->name).'</option>';
		
		$selected1 = ( $result == 0 ) ? 'selected="selected"' : '';
	}

			
	?>
   
<tr class="form-field term-parent-wrap">
			<th scope="row"><label for="parent"><?php _e( 'Select Template', 'redux-framework' ); ?></label></th>
			<td>
					<select name="_sb_ad_template">	
			<option value=""><?php echo __('Select Option', 'redux-framework');?></option>
			<option value="0" <?php echo $selected1; ?></optio><?php echo __('Default Template', 'redux-framework');?></option>
			<?php echo ($optionsHtml);?>	
		</select>  
			<p class="description"><?php echo __('You can assign this template to only 1st level category. It will not work with sub categories.', 'redux-framework');?></p>			
							<br /></td>
		</tr>

	<?php
}



add_action( 'ad_cats_add_form_fields', 'sb_add_template_to_cat', 10, 2 );
add_action( 'ad_cats_edit_form_fields', 'sb_add_template_to_cat', 10, 2 );


/* Asign template to category */
function adforest_assign_template_to_category( $term_id ) {
	if ( isset( $_POST ) && isset($_POST['_sb_ad_template']) && $_POST['_sb_ad_template'] != "" ) {
			
		$templateID = (isset($_POST['_sb_ad_template']) && $_POST['_sb_ad_template'] > 0) ? $_POST['_sb_ad_template'] : '';
		//if( $templateID != "" ){
			update_term_meta( $term_id, '_sb_category_template', $templateID );	
		//}
	}
}  
add_action( 'create_ad_cats', 'adforest_assign_template_to_category');
add_action( 'edit_ad_cats', 'adforest_assign_template_to_category');


/*For Front End*/

function adforest_dynamic_templateID($cat_id)
{
	$termTemplate = '';
	if($cat_id != "")
	{	

		$termTemplate	= get_term_meta( $cat_id , '_sb_category_template' , true);
		
		$go_next = ($termTemplate == "" || $termTemplate == 0) ? true : false;
		if($go_next)
		{
			$parent = get_term($cat_id);
			if( $parent->parent > 0)
			{
				$cat_id = $parent->parent;
				$termTemplate	= get_term_meta( $cat_id , '_sb_category_template' , true);
				
				$go_next = ($termTemplate == "" || $termTemplate == 0) ? true : false;
				$parent = get_term($cat_id);
				if( $parent->parent > 0 && $go_next)
				{
					$cat_id = $parent->parent;
					$termTemplate	= get_term_meta( $cat_id , '_sb_category_template' , true);
					$parent = get_term($cat_id);
					$go_next = ($termTemplate == "" || $termTemplate == 0) ? true : false;
					if( $parent->parent > 0  && $go_next )
					{
						$cat_id = $parent->parent;
						$termTemplate	= get_term_meta( $cat_id , '_sb_category_template' , true);
						$parent = get_term($cat_id);
						$go_next = ($termTemplate == "" || $termTemplate == 0) ? true : false;
						if( $parent->parent > 0  && $go_next)
						{
							$cat_id = $parent->parent;
							$termTemplate	= get_term_meta( $cat_id , '_sb_category_template' , true);
							$parent = get_term($cat_id);
							$go_next = ($termTemplate == "" || $termTemplate == 0) ? true : false;
							if( $parent->parent > 0  && $go_next )
							{
								$cat_id = $parent->parent;
								$termTemplate	= get_term_meta( $cat_id , '_sb_category_template' , true);
							}							
						}						
					}					
				}				
			}
		}
	}
	
	return $termTemplate;
	
}
//Dynamic Fields starts
add_action('wp_ajax_sb_get_sub_template', 'adforest_post_ad_fields');
function adforest_post_ad_fields()
{

	$html  			= $termTemplate = '';
	$id 			= isset($_POST['is_update']) ? $_POST['is_update'] : '';
	$cat_id 		= isset($_POST['cat_id']) ? $_POST['cat_id'] : '';

	$termTemplate = adforest_dynamic_templateID($cat_id);
		
	$html  .= adforest_get_static_form($termTemplate, $id);
	$html  .= adforest_get_dynamic_form($termTemplate, $id);	
	echo ($html);
	die();
}
//Dynamic fields ends

function adforest_returnHTML($id = '')
{
	
	if($id == "") return '';
	$html = '';
	
	
	 $cats	=	adforest_get_ad_cats ( $id );
	 foreach( $cats as $cat )
	 {
	 	$mainCatId = $cat['id'];
	 }
	 /*$termTemplate = 	get_term_meta( $mainCatId , '_sb_category_template' , true);*/
	 $termTemplate = adforest_dynamic_templateID($mainCatId);
	 $html .=	adforest_get_static_form($termTemplate, $id);
	 $html .=	adforest_get_dynamic_form($termTemplate, $id);
	
	
	return $html;
	
}

function adforest_get_dynamic_form($term_id = '', $post_id = '')
{
	$html = '';
	
	if($term_id == '') return $html;
	$result = get_term_meta( $term_id , '_sb_dynamic_form_fields' , true);
	if(isset($result) && $result != "")
	{
		$formData = sb_dynamic_form_data($result);	
		
		foreach($formData as $data)
		{	
			$status   = ($data['status']);
			
			if( isset($status) && $status == 1 )
			{
			$types    = ($data['types']);
			$titles   = ($data['titles']);			
			$key	  = '_adforest_tpl_field_'.$data['slugs'];
			$slugs   = 'cat_template_field['. $key . ']';			
			$values   = ($data['values']);			
			$columns   = ($data['columns']);
			$requires = '';
			$required_html	=	'';
			if(isset($data['requires']) && $data['requires'] == '1')
			{
				$required_html	=	'<span class="required"> *</span>';
				$message = 'data-parsley-error-message="'. __( 'This field is required.', 'adforest' ) .'"';
				$requires = 'selected="selected" data-parsley-required="true" '. $message; 
			}
		
			$fieldValue = (isset( $post_id ) &&  $post_id!= "") ?  get_post_meta($post_id, $key, true ) : '';
			
			if($types == 1){

			$html .=	'
				  <div class="col-md-'.$columns.' col-lg-'.$columns.' col-xs-12 col-sm-12 margin-bottom-20">
					 <label class="control-label">'.esc_html($titles). $required_html .'</label>
					 <input class="form-control" name="'.esc_attr($slugs).'" value="'.$fieldValue.'" type="text" '.$requires.'>
				  </div>
			  ';
			}
			$options = '';
			if($types == 2){

			$vals = @explode("|", $values );	
			
			foreach($vals as $val)
			{
				$selected = ($fieldValue == $val) ? 'selected="selected"' : '';
				$options .= '<option value="'.esc_html($val).'" '.$selected.'>'.esc_html($val).'</option>';
			}
			
			$html .= '
			  <div class="col-md-'.$columns.' col-lg-'.$columns.' col-xs-12 col-sm-12 margin-bottom-20">
				 <label class="control-label">'.esc_html($titles). $required_html . '</label>
				 <select class="category form-control" name="'.esc_attr($slugs).'" '.$requires.'>
					<option value="">'.__("Select Option", "adforest").'</option>
					'.$options.'
				 </select>
			  </div>';
			}
				if($types == 3){
					$options = '';
					$vals = @explode("|", $values ); 
					$loop =1;
					
					$fieldValue = json_decode($fieldValue, true);
					
				   
					   
					foreach($vals as $val)
					{
					 $checked =  '';
					 if( isset( $fieldValue ) && $fieldValue != "" )
					 {
					  $checked = in_array($val, $fieldValue) ? 'checked="checked"' : ''; 
					 }
						 
					 $options .= '<li><input type="checkbox" id="minimal-checkbox-'.$loop.'-'.$slugs.'"  value="'.esc_html($val).'" '.$checked.' name="'.esc_attr($slugs).'['.$val.']"><label for="minimal-checkbox-'.$loop.'-'.$slugs.'">'.esc_html($val).'</label></li>';
					 $loop++;
					}
					  
				   $html .=  '
					 <div class="col-md-'.$columns.' col-lg-'.$columns.' col-xs-12 col-sm-12 margin-bottom-20">
					<label class="control-label">'.esc_html($titles).'</label>
					 <div class="skin-minimal"><ul class="list">'.$options.'</ul></div>
					 </div>';
			   }
				/*For Date Field*/   
				if($types == 4){
	
				$html .=	'
					  <div class="col-md-'.$columns.' col-lg-'.$columns.' col-xs-12 col-sm-12 margin-bottom-20 calendar-div">
						 <label class="control-label">'.esc_html($titles).'</label>
						 <input class="form-control dynamic-form-date-fields" name="'.esc_attr($slugs).'" value="'.$fieldValue.'" type="text" '.$requires.'><i class="fa fa-calendar"></i>
					  </div>
				  ';
				}
				/*For Website URL*/   
				if($types == 5){
				$valid_message = __("Please enter a valid website URL", "redux-framework");
				$html .=	'
					  <div class="col-md-'.$columns.' col-lg-'.$columns.' col-xs-12 col-sm-12 margin-bottom-20 ">
						 <label class="control-label">'.esc_html($titles).'</label>
						 <input class="form-control" name="'.esc_attr($slugs).'" value="'.$fieldValue.'" type="url" '.$requires.' data-required-message="'.esc_attr($valid_message).'" data-parsley-type="url" >
					  </div>
				  ';
				}       		
			   
   			}/*Status ends*/
		}
		
	}	
	return '<div class="row">'.$html. '</div>';
	
}


function adforest_return_input($type = 'textfield',$post_id = '', $term_id = '', $vals = array())
{
	$html			= '';
	$post_id 		= $post_id;
	$term_id 		= $term_id;
	$post_meta		= isset($vals['post_meta']) ? $vals['post_meta'] : '';
	$is_show 		= isset($vals['is_show']) 	? $vals['is_show'] : '';
	$is_req 		= isset($vals['is_req']) 	? $vals['is_req'] : '';
	$title 			= isset($vals['main_title']) 	? $vals['main_title'] : '';
	$subtitle 		= isset($vals['sub_title']) 	? $vals['sub_title'] : '';
	$fieldName 		= isset($vals['field_name']) 	? $vals['field_name'] : '';
	$fieldID 		= isset($vals['field_id']) 		? $vals['field_id'] : '';
	$fieldClass		= isset($vals['field_class']) 	? $vals['field_class'] : '';
	$fieldVals 		= isset($vals['field_value']) 	? $vals['field_value'] : '';
	$fieldReq 		= isset($vals['field_req']) 	? $vals['field_req'] : '';
	$catName 		= isset($vals['cat_name']) 		? $vals['cat_name'] : '';
	$columns 		= isset($vals['columns']) 		? $vals['columns'] : '6';
	$dataType 		= isset($vals['data-parsley-type']) 		? $vals['data-parsley-type'] : '';
	$dataMsg 		= isset($vals['data-parsley-message']) 		? $vals['data-parsley-message'] : '';
	$result 		= get_term_meta( $term_id , '_sb_dynamic_form_fields' , true);
	$showField 		= sb_custom_form_data($result, $is_show);
	$reqField 		= sb_custom_form_data($result, $is_req);
	$req 			= ($reqField == 1) ? 'true' : 'false';
	$dataTypes 		= ($dataType != '') ? 'data-parsley-type="'.$dataType.'" ' : '';
	$required 		= 'data-parsley-required="'.$req.'" '.$dataTypes.' data-parsley-error-message="'.$dataMsg .'"';
	
	$showField 		= ($term_id == "") ? 1 : $showField;
	
	$required_html	=	'';
	if( Redux::getOption('adforest_theme', 'design_type') == 'modern' )
	{
		if( $req == 'true' )
		{
			$required_html	=	'<span class="required"> *</span>';
		}
	}
	
	$small_html	=	'';
	if( $subtitle != "" )
	{
		$small_html = ' <small>'. $subtitle . '</small>';
	}
	
	if($type == 'textfield' && $showField == 1)
	{
		if($post_meta != "")
		{
			$fieldVals = get_post_meta($post_id, $post_meta, true);	
		}
		else
		{
			$tags_array = wp_get_object_terms( $post_id,  $catName, array('fields' => 'names') );
			$fieldVals	=	implode( ',', $tags_array );	
		}
		
		if( $fieldName == 'ad_price')
		{
			$html .= '<div class="row">
					<div class="col-md-'.$columns.' col-lg-'.$columns.' col-xs-12 col-sm-12">
					<label class="control-label">'. $title . $small_html .$required_html .'</label>
					<input class="form-control '.$fieldClass.'" name="'.$fieldName.'" id="'.$fieldID.'" value="'.$fieldVals.'"  data-parsley-required="true" data-parsley-pattern="/^[0-9]+\.?[0-9]*$/" data-parsley-error-message="'. __( 'only numbers allowed.', 'adforest' ) .'" />
					</div>
				</div>';	
		}
		else
		{
		
	$html .= '<div class="row">
			<div class="col-md-'.$columns.' col-lg-'.$columns.' col-xs-12 col-sm-12">
			<label class="control-label">'. $title . $small_html .$required_html .'</label>
			<input class="form-control '.$fieldClass.'" name="'.$fieldName.'" id="'.$fieldID.'" value="'.$fieldVals.'" '.$required.' />
			</div>
		</div>';	
		}

	}
	if($type == 'image' && $showField == 1)
	{
		
		$html .= '<div class="row">
			  <div class="col-md-'.$columns.' col-lg-'.$columns.' col-xs-12 col-sm-12">
			  	<label class="control-label">'. $title . $small_html.'</small></label>
				 <div id="'.$fieldID.'" class="'.$fieldClass.'" '.$required.'></div>
			  </div>
		   </div>';
		
	}
	
	
	if($type == 'select' && $showField == 1)
	{	
		$optHtml  = '';
		$selected	=	'';
		$selected	=	'';
		$fieldVals = get_post_meta($post_id, $post_meta, true);
		global $adforest_theme;
		$conditions	=	adforest_get_cats($catName , 0 );
		foreach( $conditions as $con )
		{
			$selected	=	'';
			if( $fieldName == 'ad_currency' && $post_id == "")
			{
				if( isset( $adforest_theme['sb_multi_currency_default'] ) && $adforest_theme['sb_multi_currency_default'] != ""  )
				{
					if( $adforest_theme['sb_multi_currency_default'] == $con->term_id )
					{
						$selected = ' selected="selected"';
					}
				}
				else
				{
					$selected	=	( $fieldVals == $con->name ) ? $selected = 'selected="selected"' : '';
				}	
			}
			else
			{
				$selected	=	( $fieldVals == $con->name ) ? $selected = 'selected="selected"' : '';		
			}
			$optHtml	.='<option value="'.$con->term_id.'|'.$con->name.'"'.$selected.'>'. $con->name . '</option>';
		}		
		$html	.= '<div class="row" >
			  <div class="col-md-'.$columns.' col-lg-'.$columns.' col-xs-12 col-sm-12">
			  <label class="control-label">'. $title . $small_html . $required_html . '</label>
			  <select class="'.$fieldClass.' form-control" id="'.$fieldID.'" name="'.$fieldName.'" '.$required.'>
			  <option value="">'. __('Select Option', 'adforest' ) . '</option>'.$optHtml.'</select></div></div>';	

	}
	
	if($type == 'select_custom' && $showField == 1)
	{ 
		$optHtml  = '';
		
		$fieldValz = get_post_meta($post_id, $post_meta, true);  
		$conditions = $fieldVals;
		foreach( $conditions as $key => $val )
		{
			$selected = ( $fieldValz == $key ) ? $selected = 'selected="selected"' : '';   
			$optHtml .='<option value="'.$key.'"'.$selected.'>'. $val . '</option>';
		}  
		
		$html .= '<div class="row" >
		<div class="col-md-'.$columns.' col-lg-'.$columns.' col-xs-12 col-sm-12">
		<label class="control-label">'. $title . $small_html . $required_html .'</label>
		<select class="'.$fieldClass.' form-control" id="'.$fieldID.'" name="'.$fieldName.'" '.$required.'>
		<option value="">'. __('Select Option', 'adforest' ) . '</option>'.$optHtml.'</select></div></div>'; 
	
	}
	
	return $html;

}