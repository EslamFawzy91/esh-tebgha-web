<?php
 /*Theme Options For AdForest WordPress API Theme*/   
if ( ! class_exists( 'Redux' ) )  return;
$opt_name = "adforestAPI";
$theme = wp_get_theme();
$args = array(
	'opt_name' => 'adforestAPI',
	'dev_mode' => false,
	'display_name' => __( 'AdForest Apps API Options', "adforest-rest-api" ),
	'display_version' => '1.0.0',
	'page_title' => __( 'AdForest Apps API Options', "adforest-rest-api" ),
	'update_notice' => TRUE,
	'admin_bar' => TRUE,
	'menu_type' => 'submenu',
	'menu_title' => __( 'Apps API Options', "adforest-rest-api" ),
	'allow_sub_menu' => TRUE,
	'page_parent_post_type' => 'your_post_type',
	'customizer' => TRUE,
	'default_show' => TRUE,
	'default_mark' => '*',
	'hints' => array( 'icon_position' => 'right', 'icon_size' => 'normal', 'tip_style' => array('color' => 'light', ),
	'tip_position' => array( 'my' => 'top left', 'at' => 'bottom right',  ),
	'tip_effect' => array( 'show' => array( 'duration' => '500', 'event' => 'mouseover', ),
	'hide' => array( 'duration' => '500', 'event' => 'mouseleave unfocus',  ),),
 ),
	'output' => TRUE,
	'output_tag' => TRUE,
	'settings_api' => TRUE,
	'cdn_check_time' => '1440',
	'compiler' => TRUE,
	'global_variable' => 'adforestAPI',
	'page_permissions' => 'manage_options',
	'save_defaults' => TRUE,
	'show_import_export' => TRUE,
	'database' => 'options',
	'transient_time' => '3600',
	'network_sites' => TRUE,
);



    $args['share_icons'][] = array(
        'url'   => 'https://www.facebook.com/scriptsbundle',
        'title' => __( 'Like us on Facebook', "adforest-rest-api" ),
        'icon'  => 'el el-facebook'
    );

    Redux::setArgs( $opt_name, $args );
	
	/* ------------------ App Settings ----------------------- */
    Redux::setSection( $opt_name, array(
        'title'      => __( 'App Settings', "adforest-rest-api" ),
        'id'         => 'api_app_settings',
        'desc'       => '',
        'icon' => 'el el-cogs',
        'fields'     => array(

			array(
                'id'       => 'app_is_open',
                'type'     => 'switch',
                'title'    => __( 'Make App Open', "adforest-rest-api" ),
				'desc'     => __( 'Make App Open For Public', "adforest-rest-api" ),
                'default'  => false,
            ),
			
            array(
                'id'       => 'app_logo',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Logo', 'adforest-rest-api' ),
                'compiler' => 'true',
                'desc'     => __( 'Site Logo image for the site.', 'adforest-rest-api' ),
                'subtitle' => __( 'Dimensions: 230 x 40', 'adforest-rest-api' ),
                'default'  => array( 'url' => ADFOREST_API_PLUGIN_URL."images/logo.png" ),
            ),			
			
			array(
                'id'       => 'app_settings_rtl',
                'type'     => 'switch',
                'title'    => __( 'RTL', "adforest-rest-api" ),
				'desc'     => __( 'Make app RTL', "adforest-rest-api" ),
                'default'  => false,
            ),
			array(
                'id'       => 'gmap_lang',
                'type'     => 'text',
                'title'    => __( 'App Language', 'adforest-rest-api' ),
				'desc' => adforestAPI_make_link ( 'https://developers.google.com/maps/faq#languagesupport' , __( 'List of available languages.' , 'adforest-rest-api' ) ). __( 'If you have selected RTL put language code here like for arabic ar', "adforest-rest-api" ),
				'default'  => 'en',
            ),			
		 array(
                'id'       => 'app_settings_color',
                'type'     => 'button_set',
                'title'    => __( 'App Colors', 'adforest-rest-api' ),
                'options'  => array(
                    '#f58936' => __('Default','adforest-rest-api'),
                    '#e74c3c' => __('Red','adforest-rest-api'),
                    '#00a651' => __('Green','adforest-rest-api'),
                    '#0083c9' => __('Blue','adforest-rest-api'),
                    '#7dba21' => __('Sea Green','adforest-rest-api'),
                ),
                'default'  => '#f58936'
            ),
			
            array(
                'id'       => 'app_settings_pages',
                'type'     => 'select',
                'data'     => 'pages',			
                'multi'    => true,
				'sortable' => true,
                'title'    => __( 'Select Pages', 'adforest-rest-api' ),
            ),				


			
            array(
                'id'       => 'sb_user_dp',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Default user picture', 'adforest-rest-api' ),
                'compiler' => 'true',
                'subtitle' => __( 'Dimensions: 200 x 200', 'adforest-rest-api' ),
                'default'  => array( 'url' => ADFOREST_API_PLUGIN_URL."images/user.jpg" ),
            ),	
            array(
                'id'       => 'sb_user_guest_dp',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Default guest picture', 'adforest-rest-api' ),
                'compiler' => 'true',
                'subtitle' => __( 'Dimensions: 200 x 200', 'adforest-rest-api' ),
                'default'  => array( 'url' => ADFOREST_API_PLUGIN_URL."images/user.jpg" ),
            ),					

			array(
				'id'       => 'sb_location_type',
				'type'     => 'button_set',
				'title'    => __( 'Address Type', 'adforest-rest-api' ),
				'options'  => array(
					'cities' => __('Cities', 'adforest-rest-api' ),
					'regions' => __('Adresses', 'adforest-rest-api' ),
				),
				'default'  => 'cities'
			),
			array(
                'id'       => 'app_settings_message_firebase',
                'type'     => 'switch',
                'title'    => __( 'Message Settings', "adforest-rest-api" ),
				'desc'     => __( 'Send message notification through firebase when receive new message on ad.', "adforest-rest-api" ),
                'default'  => true,
            ),	
			
			array(
                'id'       => 'app_settings_fb_btn',
                'type'     => 'switch',
                'title'    => __( 'Facebook Login/Register', "adforest-rest-api" ),
				'desc'     => __( 'Show or hide google button.', "adforest-rest-api" ),
                'default'  => true,
            ),				
			array(
                'id'       => 'app_settings_google_btn',
                'type'     => 'switch',
                'title'    => __( 'Google Login/Register', "adforest-rest-api" ),
				'desc'     => __( 'Show or hide google button.', "adforest-rest-api" ),
                'default'  => true,
            ),	
			array(
                'id'       => 'sb_enable_social_links',
                'type'     => 'switch',
                'title'    => __( 'Enable Social Profiles', 'adforest-rest-api' ),
                'subtitle' => __( 'for display', 'adforest-rest-api' ),
                'default'  => false,
            ),		
			
		
			array(
                'id'       => 'sb_enable_featured_slider_scroll',
                'type'     => 'switch',
                'title'    => __( 'Enable Scroll On Featured Ads', 'adforest-rest-api' ),
                'desc' => __( 'Turn on/off auto scroll on the featured ads slider', 'adforest-rest-api' ),
                'default'  => false,
            ),		
		
										
         	array(
                'id'       => 'sb_enable_featured_slider_duration',
                'type'     => 'text',
                'title'    => __( 'Slider Scroll Speed', 'adforest-rest-api' ),
				'default'  => 40,	
				'required' => array( 'sb_enable_featured_slider_scroll', '=', true ),		
				'desc'     => __( 'Enter value in milisecons 1000 is 1 second', 'adforest-rest-api' ),	
            ),	
         	array(
                'id'       => 'sb_enable_featured_slider_loop',
                'type'     => 'text',
                'title'    => __( 'Slider Loop scroll', 'adforest-rest-api' ),
				'default'  => 2000,	
				'required' => array( 'sb_enable_featured_slider_scroll', '=', true ),		
				'desc'     => __( 'Enter value in milisecons 1000 is 1 second. Once end starts again after X number of seconds', 'adforest-rest-api' ),	
            ),

			array(
                'id'       => 'allow_near_by',
                'type'     => 'switch',
                'title'    => __( 'Nearby Option', "adforest-rest-api" ),
				'desc'     => __( 'Turn on/off nearby option in app', "adforest-rest-api" ),
                'default'  => false,
            ),
	
	
			array(
                'id'       => 'allow_app_rating',
                'type'     => 'switch',
                'title'    => __( 'App Rating', "adforest-rest-api" ),
				'desc'     => __( 'Show app rating icon on the top.', "adforest-rest-api" ),
                'default'  => false,
            ),
	
         array(
                'id'       => 'allow_app_rating_title',
                'type'     => 'text',
                'title'    => __( 'Rating Title', 'adforest-rest-api' ),
				'default'  => __("App Store Rating", "adforest-rest-api"),	
				'required' => array( 'allow_app_rating', '=', '1' ),		
				'desc'     => __( 'Rating title in the popup.', 'adforest-rest-api' ),	
            ),	
         			
         array(
                'id'       => 'allow_app_rating_url',
                'type'     => 'text',
                'title'    => __( 'App URL', 'adforest-rest-api' ),
				'default'  => '',	
				'required' => array( 'allow_app_rating', '=', '1' ),		
				'desc'     => __( 'Enter app URL for app rating. URL is required', 'adforest-rest-api' ),	
            ),	
			
			array(
                'id'       => 'allow_app_share',
                'type'     => 'switch',
                'title'    => __( 'App Share', "adforest-rest-api" ),
				'desc'     => __( 'Show app share icon on the top.', "adforest-rest-api" ),
                'default'  => false,
            ),
         array(
                'id'       => 'allow_app_share_title',
                'type'     => 'text',
                'title'    => __( 'Share Popup Title', 'adforest-rest-api' ),
				'default'  => __("Share this", "adforest-rest-api"),	
				'required' => array( 'allow_app_share', '=', '1' ),		
				'desc'     => __( 'title in the popup.', 'adforest-rest-api' ),	
            ),
         array(
                'id'       => 'allow_app_share_text',
                'type'     => 'text',
                'title'    => __( 'Subject', 'adforest-rest-api' ),
				'default'  => '',	
				'required' => array( 'allow_app_share', '=', '1' ),		
				'desc'     => __( 'App share subject. Not required.', 'adforest-rest-api' ),	
            ),			
         array(
                'id'       => 'allow_app_share_url',
                'type'     => 'text',
                'title'    => __( 'App Share URL', 'adforest-rest-api' ),
				'default'  => '',	
				'required' => array( 'allow_app_share', '=', '1' ),		
				'desc'     => __( 'Enter app share URL for app sharing. URL is required.', 'adforest-rest-api' ),	
            ),										 
											

		)
		) );
			
Redux::setSection( $opt_name, array(
        'title'      => __( 'App Key Settings', "adforest-rest-api" ),
        'id'         => 'api_key_settings',
        'desc'       => '',
        'icon' => 'el el-key',
        'fields'     => array(

            array(
                'id'     => 'api_key_settings-info1',
                'type'   => 'info',
                'notice' => false,
                'style'  => 'info',
                'title' => __( 'Alert', 'adforest-rest-api' ),
                'desc'  => __( 'Once added be carefull editing next time. Those Key Should Be Same In App Header.', 'adforest-rest-api' )
            ),			


            array(
                'id'     => 'api_key_settings-info1-1',
                'type'   => 'info',
                'notice' => false,
                'style'  => 'info',
                'title' => __( 'Info', 'adforest-rest-api' ),
                'desc'  => __( 'Below section is only for if you have purchased Android App. Then turn it on and enter the purchase code in below text field that will appears.', 'adforest-rest-api' )
            ),	
			array(
                'id'       => 'api-is-buy-android-app',
                'type'     => 'switch',
                'title'    => __( 'For Android App', 'adforest-rest-api' ),
                'default'  => false,
				'desc'          => __( 'If you have purchased the android app.', 'adforest-rest-api' ),
            ),	
						
         array(
		 		'required' 		=> array( 'api-is-buy-android-app', '=', true ),
                'id'       => 'appKey_pCode',
                'type'     => 'text',
                'title'    => __( 'Enter You Android Purchase Code Here', 'adforest-rest-api' ),
				'default'  => '',
				'desc'  => __( 'Your android item purchase code got from codecanyon. You have purchased the item seprately.', 'adforest-rest-api' ),
                'text_hint' => array(
                    'title'   => __( 'Alert', 'adforest-rest-api' ),
                    'content' => __( 'Once added be carefull editing next time. This key Should be same in app header.' ),
                ),
							
            ),
         array(
		 		'required' 		=> array( 'api-is-buy-android-app', '=', true ),
                'id'       => 'appKey_Scode',
                'type'     => 'text',
                'title'    => __( 'Enter Your Android Secret Code Here', 'adforest-rest-api' ),
				'default'  => '',
                'text_hint' => array(
                    'title'   => __( 'Alert', 'adforest-rest-api' ),
                    'content' => __( 'Once added be carefull editing next time. This key Should be same in app header.' ),
                ),
				'desc'  => __( 'Just a random number generated by you for app security.', 'adforest-rest-api' ),			
				
            ),				
            array(
                'id'     => 'api_key_settings-info1-2',
                'type'   => 'info',
                'notice' => false,
                'style'  => 'info',
                'title' => __( 'Info', 'adforest-rest-api' ),
                'desc'  => __( 'Below section is only for if you have purchased IOS App. Then turn it on and enter the purchase code in below text field that will appears.', 'adforest-rest-api' )
            ),	
			array(
                'id'       => 'api-is-buy-ios-app',
                'type'     => 'switch',
                'title'    => __( 'For IOS App', 'adforest-rest-api' ),
                'default'  => false,
				'desc'          => __( 'If you have purchased the ios app.', 'adforest-rest-api' ),
            ),			
			
						
         array(
		 		'required' 		=> array( 'api-is-buy-ios-app', '=', true ),
                'id'       => 'appKey_pCode_ios',
                'type'     => 'text',
                'title'    => __( 'Enter You IOS Purchase Code Here', 'adforest-rest-api' ),
				'default'  => '',
				'desc'  => __( 'Your IOS item purchase code got from codecanyon. You have purchased the item seprately.', 'adforest-rest-api' ),
                'text_hint' => array(
                    'title'   => __( 'Alert', 'adforest-rest-api' ),
                    'content' => __( 'Once added be carefull editing next time. This key Should be same in app header.' ),
                ),
							
            ),			
         array(
		 		'required' 		=> array( 'api-is-buy-ios-app', '=', true ),
                'id'       => 'appKey_Scode_ios',
                'type'     => 'text',
                'title'    => __( 'Enter Your IOS Secret Code Here', 'adforest-rest-api' ),
				'default'  => '',
                'text_hint' => array(
                    'title'   => __( 'Alert', 'adforest-rest-api' ),
                    'content' => __( 'Once added be carefull editing next time. This key Should be same in app header.' ),
                ),
				'desc'  => __( 'Just a random number generated by you for app security.', 'adforest-rest-api' ),			
				
            ),	
			
            array(
                'id'     => 'api_key_settings-info1-3',
                'type'   => 'info',
                'notice' => false,
                'style'  => 'info',
                'title' => __( 'Info', 'adforest-rest-api' ),
                'desc'  => __( 'Below section is Other API and Payment settings', 'adforest-rest-api' )
            ),			
         array(
                'id'       => 'appKey_stripeKey',
                'type'     => 'text',
                'title'    => __( 'Enter Your Stripe Publishable key Here', 'adforest-rest-api' ),
				'default'  => '',
				'desc'  => __( 'This will use in app', 'adforest-rest-api' ),
                'text_hint' => array(
                    'title'   => __( 'Alert', 'adforest-rest-api' ),
                    'content' => __( 'Once added be carefull editing next time. This key Should be same in app header.' ),
                )				
				
            ),	
         array(
                'id'       => 'appKey_stripeSKey',
                'type'     => 'text',
                'title'    => __( 'Enter Your Stripe Secret key Here', 'adforest-rest-api' ),
				'default'  => '',
				'desc'  => __( 'This will use at server for varification', 'adforest-rest-api' ),
                'text_hint' => array(
                    'title'   => __( 'Alert', 'adforest-rest-api' ),
                    'content' => __( 'Once added be carefull editing next time. This key Should be same in app header.' ),
                )				
				
            ),			
			
         array(
                'id'       => 'appKey_youtubeKey',
                'type'     => 'text',
                'title'    => __( 'Enter Your Youtube Key', 'adforest-rest-api' ),
				'default'  => '',
                'text_hint' => array(
                    'title'   => __( 'Alert', 'adforest-rest-api' ),
                    'content' => __( 'Once added be carefull editing next time. This key Should be same in app header.' ),
                )				
				
            ),
			
		)
		) );			
		
		
 Redux::setSection( $opt_name, array(
        'title'      => __( 'Paypal Settings', "adforest-rest-api" ),
        'id'         => 'api_payment_paypal',
        'desc'       => '',
        'icon' => 'el el-check',
		'subsection' => true,
        'fields'     => array(
		
			array(
				'id'       => 'appKey_paypalMode',
				'type'     => 'button_set',
				'title'    => __( 'Paypal Mode', 'adforest-rest-api' ),
				'options'  => array(
					'live' => __('Live', 'adforest-rest-api' ),
					'sandbox' => __('Sandbox', 'adforest-rest-api' ),
				),
				'default'  => 'live',
			),			
			array(
				'id'       => 'appKey_paypalKey',
				'type'     => 'text',
				'title'    => __( 'Enter Your Paypal Key', 'adforest-rest-api' ),
				'default'  => '',
				'text_hint' => array(
					'title'   => __( 'Alert', 'adforest-rest-api' ),
					'content' => __( 'Once added be carefull editing next time. This key Should be same in app header.' ),
				),
				'desc'  => __( 'Enter you paypal client id here', 'adforest-rest-api' ),					
				
			),		
			
			array(
				'id'       => 'appKey_paypalClientSecret',
				'type'     => 'text',
				'title'    => __( 'Enter Your Paypal Secret', 'adforest-rest-api' ),
				'default'  => '',
				'text_hint' => array(
					'title'   => __( 'Alert', 'adforest-rest-api' ),
					'content' => __( 'Once added be carefull editing next time. This key Should be same in app header.' ),
				),
				'desc'  => __( 'Enter you paypal Secret id here', 'adforest-rest-api' ),					
				
			),
			
         array(
                'id'       => 'paypalKey_merchant_name',
                'type'     => 'text',
                'title'    => __( 'Merchant Name', 'adforest-rest-api' ),
				'default'  => '',
				'desc'  => __( 'Enter the merchant name', 'adforest-rest-api' ),	
            ),	
         array(
                'id'       => 'paypalKey_currency',
                'type'     => 'text',
                'title'    => __( 'Account Currency', 'adforest-rest-api' ),
				'default'  => '',
				'desc'  => __( 'Currency name i.e. USD Supported currency list here: ', 'adforest-rest-api' ) . ' https://developer.paypal.com/docs/integration/direct/rest/currency-codes/' ,	
            ),				
         array(
                'id'       => 'paypalKey_privecy_url',
                'type'     => 'text',
                'title'    => __( 'Privecy Url', 'adforest-rest-api' ),
				'default'  => '',
				'desc'  => __( 'Example link ', 'adforest-rest-api' ) . 'https://www.example.com/privacy',	
            ),	
         array(
                'id'       => 'paypalKey_agreement',
                'type'     => 'text',
                'title'    => __( 'Agreement Url', 'adforest-rest-api' ),
				'default'  => '',	
				'desc'  => __( 'Example link ', 'adforest-rest-api' ) . 'https://www.example.com/legal',	
            ),			           
		
		
		)
	
	));				

Redux::setSection( $opt_name, array(
        'title'      => __( 'InApp Purchase Settings', "adforest-rest-api" ),
        'id'         => 'api_payment_inapp',
        'desc'       => '',
        'icon' => 'el el-check',
		'subsection' => true,
        'fields'     => array(
		
		

			array(
                'id'       => 'api-inapp-android-app',
                'type'     => 'switch',
                'title'    => __( 'Android InApp Purchase', 'adforest-rest-api' ),
                'default'  => false,
				'desc'          => __( 'If you have purchased the android app.', 'adforest-rest-api' ),
            ),	
				

            array(
				'required' 		=> array( 'api-inapp-android-app', '=', true ),
                'id'     => 'api_inapp-info1-1',
                'type'   => 'info',
                'notice' => false,
                'style'  => 'info',
                'title' => __( 'Info', 'adforest-rest-api' ),
                'desc'  => __( 'Go to Application then you will see Development tools option on the left side of menu. Click this option now navigate to Services &APIs. Now you will Licensing & in-app billing section copy the key from here.', 'adforest-rest-api' )
            ),			
				
						
         array(
		 		'required' 		=> array( 'api-inapp-android-app', '=', true ),
                'id'       => 'inApp_androidSecret',
                'type'     => 'textarea',
                'title'    => __( 'Your Android InApp Secret Code Here', 'adforest-rest-api' ),
				'default'  => '',
				'desc'  => __( 'Enter the secret code you got from store. While copy paste please make sure there is no white space.', 'adforest-rest-api' ),
                'text_hint' => array(
                    'title'   => __( 'Alert', 'adforest-rest-api' ),
                    'content' => __( 'Once added be carefull editing next time.' ),
                ),
							
            ),		
		
	
		array(
                'id'       => 'api-inapp-ios-app',
                'type'     => 'switch',
                'title'    => __( 'AppStore InApp Purchase', 'adforest-rest-api' ),
                'default'  => false,
				'desc'          => __( 'If you have purchased the AppStore app.', 'adforest-rest-api' ),
            ),	
						
         array(
		 		'required' 		=> array( 'api-inapp-ios-app', '=', true ),
                'id'       => 'inApp_iosSecret',
                'type'     => 'textarea',
                'title'    => __( 'Your AppStore InApp Secret Code Here', 'adforest-rest-api' ),
				'default'  => '',
				'desc'  => __( 'Enter the secret code you got from store. While copy paste please make sure there is no white space.', 'adforest-rest-api' ),
                'text_hint' => array(
                    'title'   => __( 'Alert', 'adforest-rest-api' ),
                    'content' => __( 'Once added be carefull editing next time.' ),
                ),
							
            ),		
	
		
			
		
		)
	
	));	



/*
Redux::setSection( $opt_name, array(
        'title'      => __( 'PayU Settings', "adforest-rest-api" ),
        'id'         => 'api_payment_payu',
        'desc'       => '',
        'icon' => 'el el-check',
		'subsection' => true,
        'fields'     => array(
		
			array(
				'id'       => 'appKey_payuMode',
				'type'     => 'button_set',
				'title'    => __( 'PayU Mode', 'adforest-rest-api' ),
				'options'  => array(
					'live' => __('Live', 'adforest-rest-api' ),
					'sandbox' => __('Sandbox', 'adforest-rest-api' ),
				),
				'default'  => 'live',
			),			
			array(
				'id'       => 'appKey_payumarchantKey',
				'type'     => 'text',
				'title'    => __( 'Enter Your PayU marchant Key', 'adforest-rest-api' ),
				'default'  => '',
				'text_hint' => array(
					'title'   => __( 'Alert', 'adforest-rest-api' ),
					'content' => __( 'Once added be carefull editing next time.' ),
				),
				'desc'  => __( 'Enter you PayU marchant key here', 'adforest-rest-api' ),					
				
			),		
         array(
                'id'       => 'payu_salt_id',
                'type'     => 'text',
                'title'    => __( 'Salt', 'adforest-rest-api' ),
				'default'  => '',
				'desc'  => __( 'Enter salt', 'adforest-rest-api' ),	
            ),	
		
		)
	
	));	
*/



 Redux::setSection( $opt_name, array(
        'title'      => __( 'Thank You Settings', "adforest-rest-api" ),
        'id'         => 'api_payment_thankyou',
        'desc'       => '',
        'icon' => 'el el-check',
		'subsection' => true,
        'fields'     => array(
		
			array(
				'id'       => 'payment_thankyou',
				'type'     => 'text',
				'title'    => __( 'Thank You Title', 'adforest-rest-api' ),
				'default'  => __( 'Thank You For Your Order', 'adforest-rest-api' ),
			),	
		)		
	));	


			
			
	
 Redux::setSection( $opt_name, array(
        'title'      => __( 'Ads/Reporting Settings', "adforest-rest-api" ),
        'id'         => 'api_ads_screen',
        'desc'       => '',
        'icon' => 'el el-picture',
        'fields'     => array(
		
		)
	
	));			
			
 Redux::setSection( $opt_name, array(
        'title'      => __( 'Ads Settings', "adforest-rest-api" ),
        'id'         => 'api_ads_screen1',
        'desc'       => '',
		'subsection' => true,
        'fields'     => array(

            array(
                'id'    => 'opt-info-warning0',
                'type'  => 'info',
                'style' => 'warning',
                'title' => __( 'Ads Setting (AdMob)', 'adforest-rest-api' ),
                'desc'  => __( 'Here you can set the AdMob settings for the app', 'adforest-rest-api' )
            ),
			
			array(
                'id'       => 'api_ad_show',
                'type'     => 'switch',
                'title'    => __( 'Show Ads', 'adforest-rest-api' ),
                'desc'    => __( 'Trun ads on or off.', 'adforest-rest-api' ),
                'default'  => false,
            ),
			
           /* array(
                'id'       => 'api_ad_type',
                'type'     => 'button_set',
                'title'    => __( 'Add Type', 'adforest-rest-api' ),
				'required' => array( 'api_ad_show', '=', '1' ),
                'options'  => array(
                    'banner' => __( 'Banner', 'adforest-rest-api' ),
                    'interstital' => __( 'Interstital', 'adforest-rest-api' ),
                ),
				'multi'    => true,
                'default'  => 'banner'
            ),*/	
			
			
          
    		array(
                'id'       => 'api_ad_type_banner',
                'type'     => 'switch',
                'title'    => __( 'Show Banner Ads', 'adforest-rest-api' ),
                'subtitle' => __( 'Turn on or off for banner ads', 'adforest-rest-api' ),
                'default'  => false,
				'required' => array( 'api_ad_show', '=', '1' ),
            ),        
				  
			
            array(
                'id'       => 'api_ad_position',
                'type'     => 'button_set',
                'title'    => __( 'Banner Ad Position', 'adforest-rest-api' ),
				'required' => array( 'api_ad_type_banner', '=', true ),
                'options'  => array(
                    'top' => __( 'Top', 'adforest-rest-api' ),
                    'bottom' => __( 'Bottom', 'adforest-rest-api' ),
                ),
				
                'default'  => 'top'
            ),	

         array(
                'id'       => 'api_ad_key_banner',
                'type'     => 'text',
                'title'    => __( 'Enter Your Ad Key (banner) Android', 'adforest-rest-api' ),
				'default'  => '',	
				'required' => array( array( 'api-is-buy-android-app', '=', true ) , array('api_ad_type_banner', '=', true)),	
				'desc'     => __( 'Please make sure you are putting correct ad id your selected above banner', 'adforest-rest-api' ),	
            ),
			
			/*Added For IOS */		
			
         array(
                'id'       => 'api_ad_key_banner_ios',
                'type'     => 'text',
                'title'    => __( 'Enter Your Ad Key (banner) IOS', 'adforest-rest-api' ),
				'default'  => '',	
				'required' => array( array( 'api-is-buy-ios-app', '=', true ) , array('api_ad_type_banner', '=', true)),		
				'desc'     => __( 'Please make sure you are putting correct ad id your selected above banner', 'adforest-rest-api' ),	
            ),				
			/*Added For IOS   */
			
    		array(
                'id'       => 'api_ad_type_initial',
                'type'     => 'switch',
                'title'    => __( 'Show Initial Ads', 'adforest-rest-api' ),
                'subtitle' => __( 'Turn on or off for initial ads', 'adforest-rest-api' ),
                'default'  => false,
				'required' => array( 'api_ad_show', '=', '1' ),
            ),  			
						
         array(
                'id'       => 'api_ad_key',
                'type'     => 'text',
                'title'    => __( 'Enter Your Ad Key (initial) Android', 'adforest-rest-api' ),
				'default'  => '',	
				'required' => array( array( 'api-is-buy-android-app', '=', true ) , array('api_ad_type_initial', '=', true)),
				'desc'     => __( 'Please make sure you are putting correct ad id your selected above interstital', 'adforest-rest-api' ),	
            ),	
		/*For IOS */           
         array(
                'id'       => 'api_ad_key_ios',
                'type'     => 'text',
                'title'    => __( 'Enter Your Ad Key (initial) IOS', 'adforest-rest-api' ),
				'default'  => '',	
				'required' => array( array( 'api-is-buy-ios-app', '=', true ) , array('api_ad_type_initial', '=', true)),			
				'desc'     => __( 'Please make sure you are putting correct ad id your selected above interstital', 'adforest-rest-api' ),	
            ),	
		/*For IOS ends */ 		
         array(
                'id'       => 'api_ad_time_initial',
                'type'     => 'text',
                'title'    => __( 'Show 1st Ad After', 'adforest-rest-api' ),
				'default'  => '',	
				'required' => array( 'api_ad_type_initial', '=', true ),			
				'desc'     => __( 'Show 1st ad after specific time. In seconds 1 is for 1 second', 'adforest-rest-api' ),	
            ),				
         array(
                'id'       => 'api_ad_time',
                'type'     => 'text',
                'title'    => __( 'Show Ad After', 'adforest-rest-api' ),
				'default'  => '',	
				'required' => array( 'api_ad_type_initial', '=', true ),		
				'desc'     => __( 'Show ads next time after specific time. In seconds 1 is for 1 second', 'adforest-rest-api' ),	
            ),	
			

						
			
		)
		) );


 Redux::setSection( $opt_name, array(
        'title'      => __( 'Analytics Settings', "adforest-rest-api" ),
        'id'         => 'api_ads_screen2',
        'desc'       => '',
        'subsection' => true,
        'fields'     => array(

            array(
                'id'    => 'opt-info-warning1',
                'type'  => 'info',
                'style' => 'warning',
                'title' => __( 'App Analytics', 'adforest-rest-api' ),
                'desc'  => __( 'Below you can setup analytics for the app.', 'adforest-rest-api' )
            ),
			

			array(
                'id'       => 'api_analytics_show',
                'type'     => 'switch',
                'title'    => __( 'Make Analytics', 'adforest-rest-api' ),
                'desc'    => __( 'Trun ads on or off.', 'adforest-rest-api' ),
                'default'  => false,
            ),	

         array(
                'id'       => 'api_analytics_id',
                'type'     => 'text',
                'title'    => __( 'Analytics ID', 'adforest-rest-api' ),
				'default'  => '',	
				'required' => array( 'api_analytics_show', '=', true ),		
				'desc'     => __( 'Put analytics id here i.e.', 'adforest-rest-api' ). ' UA-XXXXXXXXX-X',	
            ),					
		
	
	
		)
	
	));	
			
			
 Redux::setSection( $opt_name, array(
        'title'      => __( 'Firebase Settings', "adforest-rest-api" ),
        'id'         => 'api_ads_screen3',
        'desc'       => '',
		'subsection' => true,
        'fields'     => array(

            array(
                'id'    => 'opt-info-warning2',
                'type'  => 'info',
                'style' => 'warning',
                'title' => __( 'Puch Nofifications', 'adforest-rest-api' ),
                'desc'  => __( 'Below you can setup Puch Nofifications for the app.', 'adforest-rest-api' )
            ),
         array(
                'id'       => 'api_firebase_id',
                'type'     => 'text',
                'title'    => __( 'Firebase API KEY', 'adforest-rest-api' ),
				'default'  => '',	
				'desc'     => __( 'Put firebase api key', 'adforest-rest-api' ),
            ),			
	
	
		)
	
	));			
			
	
   Redux::setSection( $opt_name, array(
        'title'      => __( 'Menu Settings', "adforest-rest-api" ),
        'id'         => 'api_menu_settings',
        'desc'       => '',
        'icon' => 'el el-align-justify',
        'fields'     => array(			
			
			array(
                'id'       => 'api-sortable-app-switch',
                'type'     => 'switch',
                'title'    => __( 'Turn Custom Menu', 'adforest-rest-api' ),
                'default'  => false,
				'desc'          => __( 'Turn on custom menu settings', 'adforest-rest-api' ),
            ),			
					
            array(
				'required' 		=> array( 'api-sortable-app-switch', '=', true ),
                'id'       => 'api-sortable-app-menu',
                'type'     => 'text',
                'title'    => __( 'Menu Title Control', 'adforest-rest-api' ),
                'desc'     => __( 'Chnage menu title to what you want.', 'adforest-rest-api' ),
                'label'    => true,
                'options'  => array(
                    'home'   			=> __("Home", "adforest-rest-api"),
                    'profile'   		=> __("Profile", "adforest-rest-api"),
                    'search' 			=> __("Advance Search", "adforest-rest-api"),
                    'messages'   		=> __("Messages", "adforest-rest-api"),
                    'my_ads'   			=> __("My Ads", "adforest-rest-api"),
                    'inactive_ads' 		=> __("Inactive Ads", "adforest-rest-api"),
                    'featured_ads'   	=> __("Featured Ads", "adforest-rest-api"),
                    'fav_ads'   		=> __("Fav Ads", "adforest-rest-api"),
                    'packages' 			=> __("Packages", "adforest-rest-api"),
					
					'pages' 			=> __("Pages", "adforest-rest-api"),
					
					'others' 			=> __("Others", "adforest-rest-api"),
					'blog' 				=> __("Blog", "adforest-rest-api"),
					'logout' 			=> __("Logout", "adforest-rest-api"),
					'login' 			=> __("Login", "adforest-rest-api"),
					'register' 			=> __("Register", "adforest-rest-api"),
					
					
                )
            ),	
			
			array(
                'id'       => 'api-menu-message-count',
                'type'     => 'switch',
                'title'    => __( 'Show Message Count', 'adforest-rest-api' ),
                'default'  => false,
				'desc'          => __( 'Turn on/off Show Message Count in menu.', 'adforest-rest-api' ),
            ),						
			
			array(
                'id'       => 'api-menu-hide-message-menu',
                'type'     => 'switch',
                'title'    => __( 'Hide Messages From Menu', 'adforest-rest-api' ),
                'default'  => true,
            ),	
			array(
                'id'       => 'api-menu-hide-package-menu',
                'type'     => 'switch',
                'title'    => __( 'Hide Package From Menu', 'adforest-rest-api' ),
                'default'  => true,
            ),							
			array(
                'id'       => 'api-menu-hide-blog-menu',
                'type'     => 'switch',
                'title'    => __( 'Hide Blog From Menu', 'adforest-rest-api' ),
                'default'  => true,
            ),			
		)
	
	));			
				
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Home Screen', "adforest-rest-api" ),
        'id'         => 'api_home_screen',
        'desc'       => '',
        'icon' => 'el el-home',
        'fields'     => array(
			
			array(
                'id'       => 'sb_home_screen_title',
                'type'     => 'text',
                'title'    => __( 'Screen Title', 'adforest-rest-api' ),
				'default'  => __( 'Home Screen', 'adforest-rest-api' ),
				'desc'       => __( 'Set the title for homescreen', 'adforest-rest-api' ),
            ),			
			
            array(
                'id'    => 'home-notice-info0',
                'type'  => 'info',
                'style' => 'info',
                'title' => __( 'Sort Home Screen Options', 'adforest-rest-api' ),
            ),	

			array(
                'id'       => 'home-screen-sortable-enable',
                'type'     => 'switch',
                'title'    => __( 'Home Sortable', 'adforest-rest-api' ),
                'default'  => false,
				'desc'          => __( 'Sort home sections here', 'adforest-rest-api' ),
            ),						
            array(
				'required' 		=> array( 'home-screen-sortable-enable', '=', true ),
                'id'       => 'home-screen-sortable',
                'type'     => 'sortable',
                'mode'     => 'checkbox', // checkbox or text
                'title'    => __( 'Sortable Sections', 'adforest-rest-api' ),
                'desc'     => __( 'Sort section layouts on homescreen', 'adforest-rest-api' ),
                'options'  => array(
                    'cat_icons' => __( 'Category Icons', 'adforest-rest-api' ),
                    'featured_ads' => __( 'Featured Slider', 'adforest-rest-api' ),
                    'latest_ads' => __( 'Latest Ads', 'adforest-rest-api' ),
					'sliders' => __( 'Category Slider Ads', 'adforest-rest-api' ),
					'cat_locations' => __( 'Locations Icons', 'adforest-rest-api' ),
					'nearby' => __( 'Nearby Ads', 'adforest-rest-api' ),
					'blogNews' => __( 'Blog/News', 'adforest-rest-api' ),
                ),
                'default'  => array(
                    'cat_icons' => __( 'Category Icons', 'adforest-rest-api' ),
					'sliders' => __( 'Simple Ads', 'adforest-rest-api' ),
				)
            ),			
			
			
			
			
            array(
                'id'       => 'api_cat_columns',
                'type'     => 'button_set',
                'title'    => __( 'Categories Columns', 'adforest-rest-api' ),
                'desc'     => __( 'Select number of info columns', 'adforest-rest-api' ),
                'options'  => array(
                    '3' => __( '3 Column', 'adforest-rest-api' ),
                    '4' => __( '4 Columns', 'adforest-rest-api' ),
                ),
                'default'  => '3'
            ),	
			
					
            array(
                'id'       => 'adforest-api-ad-cats-multi',
                'type'     => 'select',
                'data'     => 'terms',
				'args' => array(
					'taxonomies'=>'ad_cats', 'hide_empty' => false,
					/*'taxonomies' => array( 'ad_cats' ),*/
				),				
                'multi'    => true,
				'sortable' => true,
                'title'    => __( 'Categories Multi Select Option', 'adforest-rest-api' ),
                'desc'     => __( 'This is the description field, again good for additional info.', 'adforest-rest-api' ),
            ),
			
            array(
                'id'       => 'adforest-api-ad-cats-slider',
                'type'     => 'select',
                'data'     => 'terms',
				'args' => array(
					'taxonomies'=>'ad_cats', 'hide_empty' => false,
					/*'taxonomies' => array( 'ad_cats' ),*/
				),				
                'multi'    => true,
				'sortable' => true,
                'title'    => __( 'Select Slider Categories', 'adforest-rest-api' ),
            ),	
            array(
                'id'            => 'slider_ad_limit',
                'type'          => 'slider',
                'title'         => __( 'Slider Posts Limit', 'adforest-rest-api' ),
				'subtitle'    => __( 'On homepage', 'adforest-rest-api' ),
                'desc'          => __( 'Select Number of slider posts', 'adforest-rest-api' ),
                'default'       => 5,
                'min'           => 1,
                'step'          => 1,
                'max'           => 10,
                'display_value' => 'label'
            ),	






      	
		)
		) );

   /* Redux::setSection( $opt_name, array(
        'title'      => __( "Featured Ads", "adforest-rest-api" ),
        'id'         => 'api_home_latest',
        'desc'       => '',
		'subsection' => true,
        'fields'     => array(
			/*latets ads *-/

			)
		) );*/
		
    Redux::setSection( $opt_name, array(
        'title'      => __( "Featured Ads", "adforest-rest-api" ),
        'id'         => 'api_home_ads_featured',
        'desc'       => '',
		'subsection' => true,
        'fields'     => array(
			/*featured ads */
			array(
                'id'       => 'feature_on_home',
                'type'     => 'switch',
                'title'    => __( 'Featured Ads', 'adforest-rest-api' ),
                'default'  => false,
				'desc'          => __( 'Show featured ads slider', 'adforest-rest-api' ),
            ),			
			array(
                'id'       => 'sb_home_ads_title',
				'required' 		=> array( 'feature_on_home', '=', true ),
                'type'     => 'text',
                'title'    => __( 'Featured Ads Section Title', 'adforest-rest-api' ),
				'default'  => 'Featured Ads',
            ),
            array(
                'id'            => 'home_related_posts_count',
				'required' 		=> array( 'feature_on_home', '=', true ),
                'type'          => 'slider',
                'title'         => __( 'Featured Posts', 'adforest-rest-api' ),
				'subtitle'    => __( 'On homepage', 'adforest-rest-api' ),
                'desc'          => __( 'Select Number of featured posts', 'adforest-rest-api' ),
                'default'       => 5,
                'min'           => 1,
                'step'          => 1,
                'max'           => 150,
                'display_value' => 'label'
            ),				
					
			array(
				'id'       => 'home_featured_position',
				'type'     => 'button_set',
				'title'    => __( 'Featured Ads Position', 'adforest-rest-api' ),
				'options'  => array(
					'1' => __('Top', 'adforest-rest-api' ),
					'2' => __('Middle', 'adforest-rest-api' ),
					'3' => __('Bottom', 'adforest-rest-api' ),
				),
				'default'  => '1',
				'required' 		=> array( 'feature_on_home', '=', true ),
			),					

			)
		) );



    Redux::setSection( $opt_name, array(
		
        'title'      => __( "Latest Ads", "adforest-rest-api" ),
        'id'         => 'api_home_latest',
        'desc'       => '',
		'subsection' => true,
        'fields'     => array(
			/*latets ads */

           array(
                'id'    => 'home-notice-info2',
                'type'  => 'info',
                'style' => 'info',
                'title' => __( 'Latest Ads Section', 'adforest-rest-api' ),
            ),				
			array(
				'required' 		=> array( 'home-screen-sortable-enable', '=', true ),
                'id'       => 'latest_on_home',
                'type'     => 'switch',
                'title'    => __( 'Latest Ads', 'adforest-rest-api' ),
                'default'  => false,
				'desc'          => __( 'Show latest ads slider', 'adforest-rest-api' ),
            ),			
			array(
                'id'       => 'sb_home_latest_ads_title',
				'required' 		=> array( 'latest_on_home', '=', true ),
                'type'     => 'text',
                'title'    => __( 'Latest Ads Section Title', 'adforest-rest-api' ),
				'default'  => 'Latest Ads',
            ),
            array(
                'id'            => 'home_latest_posts_count',
				'required' 		=> array( 'latest_on_home', '=', true ),
                'type'          => 'slider',
                'title'         => __( 'Latest Ads', 'adforest-rest-api' ),
				'subtitle'    => __( 'On homepage', 'adforest-rest-api' ),
                'desc'          => __( 'Select Number of latest ads', 'adforest-rest-api' ),
                'default'       => 5,
                'min'           => 1,
                'step'          => 1,
                'max'           => 150,
                'display_value' => 'label'
            ),					
			)
		) );



    Redux::setSection( $opt_name, array(
		
        'title'      => __( "Near By Ads", "adforest-rest-api" ),
        'id'         => 'api_home_nearby',
        'desc'       => '',
		'subsection' => true,
        'fields'     => array(
			/*latets ads */

           array(
                'id'    => 'home-notice-nearby-info2',
                'type'  => 'info',
                'style' => 'info',
                'title' => __( 'Near By Ads Section', 'adforest-rest-api' ),
            ),				
			array(
				'required' 		=> array( 'home-screen-sortable-enable', '=', true ),
                'id'       => 'nearby_on_home',
                'type'     => 'switch',
                'title'    => __( 'Nearby Ads', 'adforest-rest-api' ),
                'default'  => false,
				'desc'          => __( 'Show near by ads slider', 'adforest-rest-api' ),
            ),			
			array(
                'id'       => 'sb_home_nearby_ads_title',
				'required' 		=> array( 'nearby_on_home', '=', true ),
                'type'     => 'text',
                'title'    => __( 'Nearby Ads Section Title', 'adforest-rest-api' ),
				'default'  => 'Near By Ads',
            ),
            array(
                'id'            => 'home_nearby_posts_count',
				'required' 		=> array( 'nearby_on_home', '=', true ),
                'type'          => 'slider',
                'title'         => __( 'Nearby Ads', 'adforest-rest-api' ),
				'subtitle'    => __( 'On homepage', 'adforest-rest-api' ),
                'desc'          => __( 'Select max number of nearby ads', 'adforest-rest-api' ),
                'default'       => 5,
                'min'           => 1,
                'step'          => 1,
                'max'           => 150,
                'display_value' => 'label'
            ),					
			)
		) );




    Redux::setSection( $opt_name, array(
        'title'      => __( "Ads Locations", "adforest-rest-api" ),
        'id'         => 'api_home_locations',
        'desc'       => '',
		'subsection' => true,
        'fields'     => array(
			/*locations*/
            array(
                'id'    => 'home-notice-info5',
                'type'  => 'info',
                'style' => 'info',
                'title' => __( 'Locations icons Section', 'adforest-rest-api' ),
            ),	
			array(
                'id'       => 'api_location_title',
				'required' 		=> array( 'home-screen-sortable-enable', '=', true ),
                'type'     => 'text',
                'title'    => __( 'Location Section Title', 'adforest-rest-api' ),
				'default'  => __( 'Locations', 'adforest-rest-api' ),
            ),						
            /*array(
				'required' 		=> array( 'home-screen-sortable-enable', '=', true ),
                'id'       => 'api_location_columns',
                'type'     => 'button_set',
                'title'    => __( 'Locations Columns', 'adforest-rest-api' ),
                'desc'     => __( 'Select number of info columns', 'adforest-rest-api' ),
                'options'  => array(
                    '1' => __( '1 Column', 'adforest-rest-api' ),
                    '2' => __( '2 Columns', 'adforest-rest-api' ),
                ),
                'default'  => '2'
            ),	*/					
            array(
				'required' 		=> array( 'home-screen-sortable-enable', '=', true ),
                'id'       => 'adforest-api-ad-loc-multi',
                'type'     => 'select',
                'data'     => 'terms',
				'args' => array(
					'taxonomies'=>'ad_country', 'hide_empty' => false,
				),				
                'multi'    => true,
				'sortable' => true,
                'title'    => __( 'Select Locations Categories', 'adforest-rest-api' ),
                'desc'     => __( 'Select locations you want to show', 'adforest-rest-api' ),
            ),

			)
		) );



    Redux::setSection( $opt_name, array(
        'title'      => __( "Blog/News", "adforest-rest-api" ),
        'id'         => 'api_home_blogNews',
        'desc'       => '',
		'subsection' => true,
        'fields'     => array(
			/*locations*/
            array(
                'id'    => 'home-notice-info7',
                'type'  => 'info',
                'style' => 'info',
                'title' => __( 'Blog and news section. (You must on Home Sortable to show blogs)', 'adforest-rest-api' ),
            ),	
			array(
				'required' 		=> array( 'home-screen-sortable-enable', '=', true ),
                'id'       => 'posts_blogNews_home',
                'type'     => 'switch',
                'title'    => __( 'News/Blog', 'adforest-rest-api' ),
                'default'  => false,
				'desc'          => __( 'Show News/Blog ads slider', 'adforest-rest-api' ),
            ),			
			array(
                'id'       => 'api_blogNews_title',
				'required' 		=> array( 'posts_blogNews_home', '=', true ),
                'type'     => 'text',
                'title'    => __( 'Blog/News Setion Title', 'adforest-rest-api' ),
				'default'  => __( 'Blog/News', 'adforest-rest-api' ),
            ),	
			

			array(
				'required' 		=> array( 'posts_blogNews_home', '=', true ),
                'id'       => 'adforest-api-blogNews-multi',
                'type'     => 'select',
                'data'     => 'terms',
				'args' => array(
					'taxonomies'=>'category', 'hide_empty' => false,
				),				
                'multi'    => true,
				'sortable' => true,
                'title'    => __( 'Select Categories', 'adforest-rest-api' ),
                'desc'     => __( 'Select categories to show in the blog/news section. Leave empty if you want to show from all.', 'adforest-rest-api' ),
            ),

            array(
                'id'            => 'home_blogNews_posts_count',
				'required' 		=> array( 'posts_blogNews_home', '=', true ),
                'type'          => 'slider',
                'title'         => __( 'Number of Posts', 'adforest-rest-api' ),
				'subtitle'    => __( 'On homepage', 'adforest-rest-api' ),
                'desc'          => __( 'Select max number of Posts to show', 'adforest-rest-api' ),
                'default'       => 5,
                'min'           => 1,
                'step'          => 1,
                'max'           => 150,
                'display_value' => 'label'
            ),					

											
            /*array(
				'required' 		=> array( 'home-screen-sortable-enable', '=', true ),
                'id'       => 'api_location_columns',
                'type'     => 'button_set',
                'title'    => __( 'Locations Columns', 'adforest-rest-api' ),
                'desc'     => __( 'Select number of info columns', 'adforest-rest-api' ),
                'options'  => array(
                    '1' => __( '1 Column', 'adforest-rest-api' ),
                    '2' => __( '2 Columns', 'adforest-rest-api' ),
                ),
                'default'  => '2'
            ),						
            */

			)
		) );

/*Home Complete Ends Here*/




    Redux::setSection( $opt_name, array(
        'title'      => __( "Ad's General Settings", "adforest-rest-api" ),
        'id'         => 'api_ad_posts',
        'desc'       => '',
        'icon' => 'el el-adjust-alt',
        'fields'     => array(			
		array(
			'id'       => 'sb_location_allowed',
			'type'     => 'switch',
			'title'    => __( 'Allowed all countries', 'adforest-rest-api' ),
			'default'  => true,
		),
		array(
			'id'       => 'sb_list_allowed_country',
			'type'     => 'select',
			'options'     => adforestAPI_get_all_countries(),
			'multi'    => false,
			'title'    => __( 'Select Countries', 'adforest-rest-api' ),
			'required' => array( 'sb_location_allowed', '=', array( '0' ) ),
			'desc'     => __( 'You can select only 1 country.', 'adforest-rest-api' ),
			),
		 	array(
                'id'       => 'communication_mode',
                'type'     => 'button_set',
                'title'    => __( 'Communications Mode', 'adforest-rest-api' ),
                'options'  => array(
                    'phone' => __('Phone', 'adforest-rest-api' ),
                    'message' => __('Messages', 'adforest-rest-api' ),
                    'both' => __('Both', 'adforest-rest-api' ),
                ),
                'default'  => 'both'
            ),
   			array(
                'id'       => 'sb_order_auto_approve',
                'type'     => 'switch',
                'title'    => __( 'Package order auto approval', 'adforest-rest-api' ),
                'subtitle'    => __( 'after payment', 'adforest-rest-api' ),
                'default'  => false,
            ),
			array(
                'id'       => 'sb_send_email_on_ad_post',
                'type'     => 'switch',
                'title'    => __( 'Send email on Ad Post', 'adforest-rest-api' ),
                'default'  => true,
            ),
			array(
                'id'       => 'ad_post_email_value',
                'type'     => 'text',
                'title'    => __( 'Email for notification.', 'adforest-rest-api' ),
				'required' => array( 'sb_send_email_on_ad_post', '=', '1' ),
				'default'  => get_option( 'admin_email' ),
            ),
			array(
                'id'       => 'sb_send_email_on_message',
                'type'     => 'switch',
                'title'    => __( 'Send email on message', 'adforest-rest-api' ),
                'desc'    => __( 'When someone drop a message on ad then email send to concern user.', 'adforest-rest-api' ),
                'default'  => true,
            ),

			array(
                'id'       => 'sb_currency',
                'type'     => 'text',
                'title'    => __( 'Currency', 'adforest-rest-api' ),
				'desc' => adforestAPI_make_link ( 'http://htmlarrows.com/currency/' , __( 'List of Currency' , 'adforest-rest-api' ) ) . " " . esc_attr__( 'You can use HTML code or text as well like USD etc', 'adforest-rest-api' ),
				'default'  => '$',
            ),
			array(
                'id'       => 'sb_price_direction',
                'type'     => 'select',
                'options'  => array('left' => 'Left', 'right' => 'Right' ),
                'title'    => __( 'Price direction', 'adforest-rest-api' ),
				'default'  => 'left',
            ),
			array(
                'id'       => 'sb_price_separator',
                'type'     => 'text',
                'title'    => __( 'Thousands Separator', 'adforest-rest-api' ),
				'default'  => ',',
            ),
			array(
                'id'       => 'sb_price_decimals',
                'type'     => 'text',
                'title'    => __( 'Decimals', 'adforest-rest-api'),
                'desc'    => __( 'It should be 0 for no decimals.', 'adforest-rest-api' ),
				'default'  => '2',
            ),
			array(
                'id'       => 'sb_price_decimals_separator',
                'type'     => 'text',
                'title'    => __( 'Decimals Separator', 'adforest-rest-api' ),
				'default'  => '.',
            ),
			array(
                'id'       => 'sb_ad_approval',
                'type'     => 'select',
                'options'  => array('auto' => 'Auto Approved', 'manual' => 'Admin manual approval' ),
                'title'    => __( 'Ad Approval', 'adforest-rest-api' ),
				'default'  => 'auto',
            ),
			array(
                'id'       => 'sb_update_approval',
                'type'     => 'select',
                'options'  => array('auto' => 'Auto Approved', 'manual' => 'Admin manual approval' ),
                'title'    => __( 'Ad Update Approval', 'adforest-rest-api' ),
				'default'  => 'auto',
            ),
			array(
                'id'       => 'email_on_ad_approval',
                'type'     => 'switch',
                'title'    => __( 'Email to Ad owner on approval', 'adforest-rest-api' ),
                'default'  => true,
            ),
			

			array(
                'id'       => 'report_options',
                'type'     => 'text',
                'title'    => __( 'Report ad Options', 'adforest-rest-api' ),
				'default'  => 'Spam|Offensive|Duplicated|Fake',
            ),
			array(
                'id'       => 'report_limit',
                'type'     => 'text',
                'title'    => __( 'Ad Report Limit', 'adforest-rest-api' ),
				'desc'     => __( 'Only integer value without spaces.', 'adforest-rest-api' ),
				'default'  => 10,
            ),
			array(
                'id'       => 'report_action',
                'type'     => 'select',
                'title'    => __( 'Action on Ad Report Limit', 'adforest-rest-api' ),
				'options'  => array(1 => 'Auto Inactive', 2 => 'Email to Admin'),
				'default'  => 1,
            ),
			array(
                'id'       => 'report_email',
                'type'     => 'text',
                'title'    => __( 'Email', 'adforest-rest-api'),
				'desc'     => __( 'Email where you want to get notify.', 'adforest-rest-api' ),
				'required' => array( 'report_action', '=', array( 2 ) ),
				'default'  => get_option( 'admin_email' ),
            ),			
            array(
                'id'       => 'default_related_image',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Default Image', 'adforest-rest-api' ),
                'compiler' => 'true',
                'desc'     => __( 'If there is no image of ad then this will be show.', 'adforest-rest-api' ),
                'subtitle' => __( 'Dimensions: 300 x 225', 'adforest-rest-api' ),
                'default'  => array( 'url' => ADFOREST_API_PLUGIN_URL."images/default-img.png" ),
            ),			
			
		 	array(
                'id'       => 'ads_images_sizes',
                'type'     => 'button_set',
                'title'    => __( 'Set Image Sizes for listings', 'adforest-rest-api' ),
                'options'  => array(
                    'default' => __('Default', 'adforest-rest-api' ),
                    'size2' => __('Size 2', 'adforest-rest-api' ),
                    'size3' => __('Size 3', 'adforest-rest-api' ),
					'size4' => __('Size 4', 'adforest-rest-api' ),
					'size5' => __('Size 5', 'adforest-rest-api' ),
                ),
                'default'  => 'default',
				'desc'     => __( 'Change with caution we only recommend default.', 'adforest-rest-api' ),
            ),	

		 	array(
                'id'       => 'ads_images_sizes_adDetils',
                'type'     => 'button_set',
                'title'    => __( 'Set Image Sizes for Ad Details', 'adforest-rest-api' ),
                'options'  => array(
                    'default' => __('Default', 'adforest-rest-api' ),
                    'size2' => __('Size 2', 'adforest-rest-api' ),
                ),
                'default'  => 'default',
				'desc'     => __( 'Change with caution we only recommend default.', 'adforest-rest-api' ),
            ),						
			
			
			)
		) );

    Redux::setSection( $opt_name, array(
        'title'      => __( "Ad Post Settings", "adforest-rest-api" ),
        'id'         => 'api_ad_post_settings',
        'desc'       => '',
        'icon' => 'el el-home',
		'subsection' => true,
        'fields'     => array(
			
			array(
                'id'       => 'adpost_cat_template',
                'type'     => 'switch',
                'title'    => __( 'Turn On Category Template', 'adforest-rest-api' ),
                'default'  => false,
            ),			
			array(
                'id'       => 'admin_allow_unlimited_ads',
                'type'     => 'switch',
                'title'    => __( 'Post unlimited free ads', 'adforest-rest-api' ),
				'subtitle'     => __( 'For Administrator', 'adforest-rest-api' ),
                'default'  => true,
            ),
			array(
                'id'       => 'sb_standard_images_size',
                'type'     => 'switch',
                'title'    => __( 'Strict image mode', 'adforest-rest-api' ),
				'subtitle'     => __( 'Not allowed less than 760x410', 'adforest-rest-api' ),
                'default'  => false,
            ),			
			array(
                'id'       => 'sb_allow_ads',
                'type'     => 'switch',
                'title'    => __( 'Free Ads', 'adforest-rest-api' ),
				'subtitle'     => __( 'For new user', 'adforest-rest-api' ),
                'default'  => true,
            ),

			array(
                'id'       => 'sb_free_ads_limit',
                'type'     => 'text',
                'title'    => __( 'Free Ads limit', 'adforest-rest-api' ),
				'required' => array( 'sb_allow_ads', '=', array( true) ),
				'subtitle'     => __( 'For new user', 'adforest-rest-api' ),
				'desc'     => __( 'It must be an inter value, -1 means unlimited.', 'adforest-rest-api' ),
				'default'  => -1,
            ),
			
			array(
                'id'       => 'sb_allow_featured_ads',
                'type'     => 'switch',
                'title'    => __( 'Free Featured Ads', 'adforest-rest-api' ),
				'subtitle'     => __( 'For new user', 'adforest-rest-api' ),
                'default'  => true,
            ),

			array(
                'id'       => 'sb_featured_ads_limit',
                'type'     => 'text',
                'title'    => __( 'Featured Ads limit', 'adforest-rest-api' ),
				'subtitle'     => __( 'For new user', 'adforest-rest-api' ),
				'required' => array( 'sb_allow_featured_ads', '=', array( true) ),
				'desc'     => __( 'It must be an inter value, -1 means unlimited.', 'adforest-rest-api' ),
				'default'  => 1,
            ),
			array(
                'id'       => 'sb_allow_bump_ads',
                'type'     => 'switch',
                'title'    => __( 'Free Bump Ads', 'adforest-rest-api' ),
				'subtitle'     => __( 'For new user', 'adforest-rest-api' ),
                'default'  => true,
            ),
			array(
                'id'       => 'sb_bump_ads_limit',
                'type'     => 'text',
                'title'    => __( 'Bump Ads limit', 'adforest-rest-api' ),
				'subtitle'     => __( 'For new user', 'adforest-rest-api' ),
				'required' => array( 'sb_allow_bump_ads', '=', array( true) ),
				'desc'     => __( 'It must be an inter value, -1 means unlimited.', 'adforest-rest-api' ),
				'default'  => 1,
            ),
			array(
                'id'       => 'sb_allow_free_bump_up',
                'type'     => 'switch',
                'title'    => __( 'Free Bump Ads for all users', 'adforest-rest-api' ),
				'subtitle'     => __( 'witout any package/restriction.', 'adforest-rest-api' ),
                'default'  => false,
            ),			



			array(
                'id'       => 'sb_package_validity',
                'type'     => 'text',
                'title'    => __( 'Free package validity', 'adforest-rest-api'),
                'subtitle'    => __( 'In days for new user', 'adforest-rest-api'),
				'required' => array( 'sb_allow_ads', '=', array( true) ),
				'desc'     => __( 'It must be an inter value, -1 means never expired.', 'adforest-rest-api'),
				'default'  => -1,
            ),
			
			array(
                'id'       => 'simple_ad_removal',
                'type'     => 'text',
                'title'    => __( 'Simple ad remove after', 'adforest-rest-api'),
				'subtitle'    => __( 'In DAYS', 'adforest-rest-api'),
				'desc'     => __( 'Only integer value without spaces -1 means never expired.', 'adforest-rest-api'),
				'default'  => -1,
            ),
			array(
                'id'       => 'featured_expiry',
                'type'     => 'text',
                'title'    => __( 'Feature Ad Expired', 'adforest-rest-api' ),
				'subtitle'    => __( 'In DAYS', 'adforest-rest-api' ),
				'desc'     => __( 'Only integer value without spaces -1 means never expired.', 'adforest-rest-api' ),
				'default'  => 7,
            ),
			array(
                'id'       => 'sb_upload_limit',
                'type'     => 'select',
                'title'    => __( 'Ad image set limit', 'adforest-rest-api' ),
				'options'  => array(1 => 1,2 => 2,3 => 3,4 => 4,5 => 5,6 => 6,7 => 7,8 => 8,9 => 9,10 => 10, 11 => 11, 12=> 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24, 25 => 25),
				'default'  => 5,
            ),
			array(
                'id'       => 'sb_upload_size',
                'type'     => 'select',
                'title'    => __( 'Ad image max size', 'adforest-rest-api' ),
				'options'  => array( '307200-300kb' => '300kb', '614400-600kb' => '600kb', '819200-800kb' => '800kb', '1048576-1MB' => '1MB', '2097152-2MB' => '2MB', '3145728-3MB' => '3MB', '4194304-4MB' => '4MB', '5242880-5MB' => '5MB', '6291456-6MB' => '6MB', '7340032-7MB' => '7MB', '8388608-8MB' => '8MB', '9437184-9MB' => '9MB', '10485760-10MB' => '10MB', '11534336-11MB' => '11MB', '12582912-12MB' => '12MB', '13631488-13MB' => '13MB', '14680064-14MB' => '14MB', '15728640-15MB' => '15MB', '20971520-20MB' => '20MB', '26214400-25MB' => '25MB' ),
				'default'  => '2097152-2MB',
            ),
			array(
                'id'       => 'allow_tax_condition',
                'type'     => 'switch',
                'title'    => __( 'Display Condition Taxonomy', 'adforest-rest-api' ),
                'default'  => true,
            ),
			array(
                'id'       => 'allow_tax_warranty',
                'type'     => 'switch',
                'title'    => __( 'Display Warranty Taxonomy', 'adforest-rest-api' ),
                'default'  => true,
            ),
			array(
                'id'       => 'allow_lat_lon',
                'type'     => 'switch',
                'title'    => __( 'Latitude & Longitude', 'adforest-rest-api' ),
				'desc'     => __( 'This will be display on ad post page for pin point map', 'adforest-rest-api' ),
                'default'  => true,
            ),
			array(
                'id'       => 'sb_default_lat',
                'type'     => 'text',
                'title'    => __( 'Latitude', 'adforest-rest-api' ),
                'subtitle' => __( 'for default map.', 'adforest-rest-api' ),
				'required' => array( 'allow_lat_lon', '=', true ),
                'default'  => '40.7127837' ,
            ),
			array(
                'id'       => 'sb_default_long',
                'type'     => 'text',
                'title'    => __( 'Longitude', 'adforest-rest-api' ),
                'subtitle' => __( 'for default map.', 'adforest-rest-api' ),
				'required' => array( 'allow_lat_lon', '=', true ),
                'default'  => '-74.00594130000002' ,
            ),
			array(
                'id'       => 'allow_price_type',
                'type'     => 'switch',
                'title'    => __( 'Price Type', 'adforest-rest-api' ),
				'desc'     => __( 'Display Price type option.', 'adforest-rest-api' ),
                'default'  => true,
            ),
		array(
				'id'       => 'sb_price_types',
				'type'     => 'select',
				'options'     => array( 
					'Fixed' => __( 'Fixed', 'adforest-rest-api' ),
					'Negotiable' => __( 'Negotiable', 'adforest-rest-api' ),
					'on_call' => __( 'Price on call', 'adforest-rest-api' ),
					'auction' => __( 'Auction', 'adforest-rest-api' ),
					'free' => __( 'Free', 'adforest-rest-api' ), 
					'no_price' => __( 'No price', 'adforest-rest-api' ),
					),
				'multi'    => true,
				'sortable' => true,
				'title'    => __( 'Price Types', 'adforest-rest-api' ),
				'default'  => array( ),
			),
				array(
					'id'       => 'sb_price_types_more',
					'type'     => 'text',
					'title'    => __( 'Custom Price Type', 'adforest-rest-api' ),
					'desc'    => __( 'Separated by | like option 1|option 2', 'adforest-rest-api' ),
					'default'  => '',
				),
				
			array(
                'id'       => 'sb_ad_update_notice',
                'type'     => 'text',
                'title'    => __( 'Update Ad Notice', 'adforest-rest-api' ),
				'default'  => 'Hey, be careful you are updating this AD.',
            ),
			array(
                'id'       => 'allow_featured_on_ad',
                'type'     => 'switch',
                'title'    => __( 'Allow make featured ad', 'adforest-rest-api' ),
				'subtitle' => __( 'on ad post.', 'adforest-rest-api' ),
                'default'  => true,
            ),
			array(
                'id'       => 'sb_feature_desc',
                'type'     => 'textarea',
                'title'    => __( 'Featured ad description', 'adforest-rest-api' ),
				'subtitle' => __( 'on ad post.', 'adforest-rest-api' ),
				'required' => array( 'allow_featured_on_ad', '=', true ),
				'default'  => 'Featured AD has more attention as compare to simple ad.',
            ),
			
			array(
                'id'       => 'bad_words_filter',
                'type'     => 'textarea',
                'title'    => __( 'Bad Words Filter', 'adforest-rest-api' ),
				'subtitle' => __( 'comma separated', 'adforest-rest-api' ),
				'placeholder'   => __( 'word1,word2', 'adforest-rest-api' ),
				'desc'     => __( 'This words will be removed from AD Title and Description', 'adforest-rest-api' ),
				'default'  => '',
            ),
			array(
                'id'       => 'bad_words_replace',
                'type'     => 'text',
                'title'    => __( 'Bad Words Replace Word', 'adforest-rest-api' ),
				'desc'     => __( 'This words will be replace with above bad words list from AD Title and Description', 'adforest-rest-api' ),
				'default'  => '',
            ),



			)
		) );

    Redux::setSection( $opt_name, array(
        'title'      => __( "Ad View Settings", "adforest-rest-api" ),
        'id'         => 'api_ad_view_settings',
        'desc'       => '',
        'icon' => 'el el-home',
		'subsection' => true,
        'fields'     => array(
		
            array(
                'id'       => 'api_ad_details_info_column',
                'type'     => 'button_set',
                'title'    => __( 'Info Columns', 'adforest-rest-api' ),
				'subtitle'    => __( 'On ad details page', 'adforest-rest-api' ),
                'desc'     => __( 'Select number of info columns', 'adforest-rest-api' ),
                'options'  => array(
                    '1' => __( '1 Column', 'adforest-rest-api' ),
                    '2' => __( '2 Columns', 'adforest-rest-api' ),
                ),
                'default'  => '2'
            ),

			array(
                'id'       => 'related_ads_on',
                'type'     => 'switch',
                'title'    => __( 'Related Ads', 'adforest-rest-api' ),
                'default'  => true,
            ),
			array(
                'id'       => 'sb_related_ads_title',
				'required' 		=> array( 'related_ads_on', '=', true ),
                'type'     => 'text',
                'title'    => __( 'Related Ads Section Title', 'adforest-rest-api'),
				'default'  => 'Similiar Ads',
            ),
            array(
                'id'            => 'api_ad_details_related_posts',
				'required' 		=> array( 'related_ads_on', '=', true ),
                'type'          => 'slider',
                'title'         => __( 'Related Posts', 'adforest-rest-api' ),
				'subtitle'    => __( 'On ad details page', 'adforest-rest-api' ),
                'desc'          => __( 'Select Number of related posts', 'adforest-rest-api' ),
                'default'       => 5,
                'min'           => 1,
                'step'          => 1,
                'max'           => 150,
                'display_value' => 'label'
            ),				
		
		
		)
	) );	
    Redux::setSection( $opt_name, array(
        'title'      => __( "Ad Search Settings", "adforest-rest-api" ),
        'id'         => 'api_ad_search_settings',
        'desc'       => '',
        'icon' => 'el el-home',
		'subsection' => true,
        'fields'     => array(
		
					
			array(
                'id'       => 'feature_on_search',
                'type'     => 'switch',
                'title'    => __( 'Featured Ads', 'adforest-rest-api' ),
                'default'  => true,
            ),
			array(
                'id'       => 'sb_search_ads_title',
				'required' 		=> array( 'feature_on_search', '=', true ),
                'type'     => 'text',
                'title'    => __( 'Featured Ads Section Title', 'adforest-rest-api' ),
				'default'  => 'Featured Ads',
            ),
            array(
                'id'            => 'search_related_posts_count',
				'required' 		=> array( 'feature_on_search', '=', true ),
                'type'          => 'slider',
                'title'         => __( 'Featured Posts', 'adforest-rest-api' ),
				'subtitle'    => __( 'On ad details page', 'adforest-rest-api' ),
                'desc'          => __( 'Select Number of featured posts', 'adforest-rest-api' ),
                'default'       => 5,
                'min'           => 1,
                'step'          => 1,
                'max'           => 150,
                'display_value' => 'label'
            ),				
		
		
		)
	) );	


	Redux::setSection( $opt_name, array(
			'title'      => __( 'Ad Rating Settings', 'adforest-rest-api' ),
			'id'         => 'sb_ad_rating_settings',
			'desc'       => '',
			'icon' => 'el el-cogs',
			'subsection' => true,
			'fields'     => array(
			
				array(
					'id'       => 'sb_ad_rating',
					'type'     => 'switch',
					'title'    => __( 'Rating on ad', 'adforest-rest-api' ),
					'default'  => false,
				),
				array(
					'id'       => 'sb_update_rating',
					'type'     => 'switch',
					'title'    => __( 'Allow update the rating', 'adforest-rest-api' ),
					'required' => array( 'sb_ad_rating', '=', array( true) ),
					'default'  => false,
				),
			  array(
					'id'       => 'sb_ad_rating_title',
					'type'     => 'text',
					'title'    => __( 'Rating section title', 'adforest-rest-api' ),
					'required' => array( 'sb_ad_rating', '=', array( true) ),
					'default'  => 'Rating & Reviews',			
				),
				array(
					'id'       => 'sb_rating_email_author',
					'type'     => 'switch',
					'title'    => __( 'Email to Author on rating', 'adforest-rest-api' ),
					'required' => array( 'sb_ad_rating', '=', array( true) ),
					'default'  => false,
				),
				array(
					'id'       => 'sb_rating_reply_email',
					'type'     => 'switch',
					'title'    => __( 'Email to Author on rating', 'adforest-rest-api' ),
					'required' => array( 'sb_ad_rating', '=', array( true) ),
					'default'  => false,
				),
				
				array(
					'id'          => 'sb_rating_max',
					'type'        => 'spinner',
					'title'    => __( 'Rating show at most', 'adforest-rest-api' ),
					'required' => array( 'sb_ad_rating', '=', array( true) ),
					'default' => '5',
					'min'     => '1',
					'step'    => '1',
					'max'     => '50',
				),
	
			
			)
		) );

	/*Only show if woocommerce plugin activated */
	if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) )
	{	
		Redux::setSection( $opt_name, array(
			'title'      => __( "Woo Products", "adforest-rest-api" ),
			'id'         => 'api_woo_products_settings',
			'desc'       => '',
			'icon' => 'el el-list-alt',
			'fields'     => array(
			
						
				array(
					'id'       => 'api_woo_products_multi',
					'type'     => 'select',
					'data'     => 'post',
					'args' => array(
						'post_type' => array( 'product' ),
					),				
					'multi'    => true,
					'sortable' => true,
					'title'    => __( 'Select Products', 'adforest-rest-api' ),
				),	
				
				
            array(
                'id'   => 'opt-info-select',
                'type' => 'info',
                'desc' => __( 'Select Payment Packages', 'adforest-rest-api' ),
            ),						

            array(
				'required' 		=> array( 'api-is-buy-android-app', '=', true ),
                'id'       => 'api-payment-packages',
                'type'     => 'select',
                'multi'    => true,
				'sortable' => true,
                'title'    => __( 'Payment Methods For Android App', 'adforest-rest-api' ),
                'desc'     => __( 'Select the payment methods you want to add.', 'adforest-rest-api' ),
                'options'  => adforestAPI_payment_types(),
                'default'  => array( 'stripe' )
            ),
			
            array(
				'required' 		=> array( 'api-is-buy-ios-app', '=', true ),
                'id'       => 'api-payment-packages-ios',
                'type'     => 'select',
                'multi'    => true,
				'sortable' => true,
                'title'    => __( 'Payment Methods IOS App', 'adforest-rest-api' ),
                'desc'     => __( 'Note ios only uses InApp Purchase', 'adforest-rest-api' ),
                'options'  => adforestAPI_payment_types('', 'ios'),
                'default'  => array( 'app_inapp' )
            ),			
			
			)
		) );	
	}
	
	
 Redux::setSection( $opt_name, array(
        'title'      => __( 'Users', "adforest-rest-api" ),
        'id'         => 'api_users_screen',
        'desc'       => '',
        'icon' => 'el el-user',
        'fields'     => array(
		
		 array(
                'id'       => 'sb_phone_verification',
				'type'     => 'switch',
                'title'    => __( 'Phone verfication', 'adforest-rest-api' ),
                'default'  => false,
				'desc'		=> __( 'If phone verification is on then system put verified batch to ad details on number so other can see this number is verified.', 'adforest-rest-api' ),
            ),
			array(
                'id'       => 'sb_resend_code',
                'type'     => 'text',
                'title'    => __( 'Resend security code', 'adforest-rest-api' ),
				'subtitle'    => __( 'In seconds', 'adforest-rest-api' ),
				'desc'     => __( 'Only integer value without spaces, 30 means 30-seconds', 'adforest-rest-api' ),
				'required' => array( 'sb_phone_verification', '=', array( '1' ) ),
				'default'  => 30,
            ),
		 array(
                'id'       => 'sb_change_ph',
				'type'     => 'switch',
                'title'    => __( 'Change phone number while ad posting.', 'adforest-rest-api' ),
                'desc'    => __( 'If off then only user profile number will be display and can not be changeable.', 'adforest-rest-api' ),
                'default'  => true,
            ),		

			
		 array(
                'id'       => 'sb_new_user_email_to_admin',
				'type'     => 'switch',
                'title'    => __( 'New User Email to Admin', 'adforest-rest-api' ),
                'default'  => true
            ),			
		
		 array(
                'id'       => 'sb_new_user_email_to_user',
				'type'     => 'switch',
                'title'    => __( 'Welcome Email to User', 'adforest-rest-api' ),
                'default'  => true
            ),	

		 array(
                'id'       => 'sb_new_user_email_verification',
				'type'     => 'switch',
                'title'    => __( 'New user email verification', 'adforest-rest-api' ),
                'default'  => false,
				'desc'		=> __( 'If verfication on then please update your new user email template by verification link.', 'adforest-rest-api' ),
            ),	
		
		
            array(
                'id'       => 'sb_new_user_register_policy',
                'type'     => 'select',
                'data'     => 'pages',			
                'multi'    => false,
				'sortable' => false,
                'title'    => __( 'Select Page', 'adforest-rest-api' ),
				'subtitle'    => __( 'Terms and Conditions', 'adforest-rest-api'),
				'desc'     => __( 'Specially for General Data Protection Regulation (GDPR)', 'adforest-rest-api' ),
            ),	
        	 array(
                'id'       => 'sb_new_user_register_checkbox_text',
                'type'     => 'text',
                'title'    => __( 'Term and Condition Text', 'adforest-rest-api' ),
				'default'  => '',
				'desc'  => __( 'Terms and Condition text next to checkbox. Leave empty if you want to show default text', 'adforest-rest-api' ),	
            ),
		 	array(
                'id'       => 'sb_new_user_delete_option',
				'type'     => 'switch',
                'title'    => __( 'Show Delete button', 'adforest-rest-api' ),
                'default'  => false,
				'desc'		=> __( 'Show delete button on user profile. Due to General Data Protection Regulation (GDPR) policy. Note: This will delete the entire data from the database and can not be recover again.', 'adforest-rest-api' ),
            ),
			
         array(
		 		'required' => array( 'sb_new_user_delete_option', '=', true ),
                'id'       => 'sb_new_user_delete_option_text',
                'type'     => 'text',
                'title'    => __( 'Delete Popup Text', 'adforest-rest-api' ),
				'default'  => 'Are you sure you want to delete the account.',
				'desc'  => __( 'Popup text after delete link clicked.', 'adforest-rest-api' ),	
            ),	
			
		 	array(
                'id'       => 'sb_user_allow_block',
				'type'     => 'switch',
                'title'    => __( 'Block User', 'adforest-rest-api' ),
                'default'  => false,
				'desc'		=> __( 'Allow users to block anyone and stop seeing his ads.', 'adforest-rest-api' ),
            ),																
						
		)
	
	));		
	
	
	
			/* ------------------Comment/Bidding Settings ----------------------- */
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Bidding Settings', 'adforest-rest-api'),
        'id'         => 'sb_comments_settings',
        'desc'       => '',
        'icon' => 'el el-cogs',
        'fields'     => array(
			array(
                'id'       => 'sb_enable_comments_offer',
                'type'     => 'switch',
                'title'    => __( 'Enable Bidding', 'adforest-rest-api'),
                'default'  => false ,
            ),
			array(
                'id'       => 'sb_enable_comments_offer_user',
                'type'     => 'switch',
                'title'    => __( 'Give bidding option to user', 'adforest-rest-api'),
				'required' => array( 'sb_enable_comments_offer', '=', '1' ),
                'default'  => false ,
            ),
			array(
                'id'       => 'bidding_timer',
                'type'     => 'switch',
                'title'    => __( 'Bidding Timer', 'adforest-rest-api' ),
				'required' => array( 'sb_enable_comments_offer', '=', '1' ),
                'default'  => false ,
            ),
			array(
				'id'       => 'top_bidder_limit',
                'type'     => 'select',
                'title'    => __( 'Top bidder limit', 'adforest-rest-api' ),
				'required' => array( 'sb_enable_comments_offer', '=', '1' ),
				'options'  => range(0,10),
				'default'  => 3,
				'desc'     => __( 'If you select 0 it will hide the tab.', 'adforest-rest-api' ),
				),			
			array(
                'id'       => 'sb_enable_comments_offer_user_title',
                'type'     => 'text',
                'title'    => __( 'User Section Title', 'adforest-rest-api'),
				'required' => array( 'sb_enable_comments_offer_user', '=', '1' ),
                'default'  => "Bidding",
            ),
			array(
                'id'       => 'sb_email_on_new_bid_on',
                'type'     => 'switch',
                'title'    => __( 'Email to Ad author', 'adforest-rest-api'),
                'subtitle'    => __( 'on bid', 'adforest-rest-api'),
				'required' => array( 'sb_enable_comments_offer', '=', '1' ),
                'default'  => false ,
            ),
			array(
                'id'       => 'sb_comments_section_title',
                'type'     => 'text',
                'title'    => __( 'Section Title', 'adforest-rest-api'),
				'required' => array( 'sb_enable_comments_offer', '=', '1' ),
                'default'  => "Bids",
            ),
			array(
                'id'       => 'sb_comments_section_note',
                'type'     => 'text',
                'title'    => __( 'Disclaimer note', 'adforest-rest-api'),
				'required' => array( 'sb_enable_comments_offer', '=', '1' ),
                'default'  => "*Your phone number will be show to post author",
            ),

        )
    ) );	

	
	/* ------------------Email Templates Settings ----------------------- */
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Email Templates', 'adforest-rest-api' ),
        'id'         => 'sb_email_templates',
        'desc'       => '',
        'icon' => 'el el-pencil',
        'fields'     => array(
          array(
                'id'       => 'sb_msg_subject_on_new_ad',
                'type'     => 'text',
                'title'    => __( 'New Ad email subject', 'adforest-rest-api' ),
                'desc'     => __( '%site_name% , %ad_owner% , %ad_title% will be translated accordingly.', 'adforest-rest-api' ),
                'default'  => 'You have new Ad - Adforest',				
            ),
          array(
                'id'       => 'sb_msg_from_on_new_ad',
                'type'     => 'text',
                'title'    => __( 'New Ad FROM', 'adforest-rest-api' ),
				'desc'     => __( 'FROM: NAME valid@email.com is compulsory as we gave in default.', 'adforest-rest-api' ),
                'default'  => 'From: '.get_bloginfo( 'name' ).' <'.get_option( 'admin_email' ).'>',				
            ),
          array(
                'id'       => 'sb_msg_on_new_ad',
                'type'     => 'editor',
                'title'    => __( 'New Ad Posted Message', 'adforest-rest-api' ),
                'desc'     => __( '%site_name% , %ad_owner% , %ad_title% , %ad_link% will be translated accordingly.', 'adforest-rest-api' ),
                'default'  => '<table class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #f6f6f6; width: 100%;" border="0" cellspacing="0" cellpadding="0"><tbody><tr><td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"></td><td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; max-width: 580px; padding: 10px; width: 580px; margin: 0 auto !important;"><div class="content" style="box-sizing: border-box; display: block; margin: 0 auto; max-width: 580px; padding: 10px;"><table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background: #fff; border-radius: 3px; width: 100%;"><tbody><tr><td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;"><table style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" border="0" cellspacing="0" cellpadding="0"><tbody><tr style="font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="alert" style="font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #000; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #fff; margin: 0; padding: 20px;" align="center" valign="top" bgcolor="#fff"><br/>A Designing and development company</td></tr><tr><td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"><span style="font-family: sans-serif; font-weight: normal;">Hello</span><span style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;"><b>Admin,</b></span></p><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">You\'ve new AD;</p><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">Title: %ad_title%</p><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">Link: <a href="%ad_link%">%ad_title%</a></p><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">Poster: %ad_owner%</p><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"><strong>Thanks!</strong></p><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">ScriptsBundle</p></td></tr></tbody></table></td></tr></tbody></table><div class="footer" style="clear: both; padding-top: 10px; text-align: center; width: 100%;"><table style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" border="0" cellspacing="0" cellpadding="0"><tbody><tr><td class="content-block powered-by" style="font-family: sans-serif; font-size: 12px; vertical-align: top; color: #999999; text-align: center;"><a style="color: #999999; text-decoration: underline; font-size: 12px; text-align: center;" href="https://themeforest.net/user/scriptsbundle">Scripts Bundle</a>.</td></tr></tbody></table></div>&nbsp;</div></td><td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"></td></tr></tbody></table>&nbsp;',				
            ),
          array(
                'id'       => 'sb_message_subject_on_new_ad',
                'type'     => 'text',
                'title'    => __( 'New Message email subject', 'adforest-rest-api' ),
                'desc'     => __( '%site_name% , %ad_title% will be translated accordingly.', 'adforest-rest-api' ),
                'default'  => 'You have new message - Adforest',				
            ),
          array(
                'id'       => 'sb_message_from_on_new_ad',
                'type'     => 'text',
                'title'    => __( 'New Message FROM', 'adforest-rest-api' ),
				'desc'     => __( 'FROM: NAME valid@email.com is compulsory as we gave in default.', 'adforest-rest-api' ),
                'default'  => 'From: '.get_bloginfo( 'name' ).' <'.get_option( 'admin_email' ).'>',				
            ),
          array(
                'id'       => 'sb_message_on_new_ad',
                'type'     => 'editor',
                'title'    => __( 'New Message template', 'adforest-rest-api' ),
                'desc'     => __( '%site_name% , %message% , %sender_name%, %ad_title% , %ad_link% will be translated accordingly.', 'adforest-rest-api' ),
                'default'  => '<table class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #f6f6f6; width: 100%;" border="0" cellspacing="0" cellpadding="0"><tbody><tr><td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"></td><td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; max-width: 580px; padding: 10px; width: 580px; margin: 0 auto !important;"><div class="content" style="box-sizing: border-box; display: block; margin: 0 auto; max-width: 580px; padding: 10px;"><table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background: #fff; border-radius: 3px; width: 100%;"><tbody><tr><td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;"><table style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" border="0" cellspacing="0" cellpadding="0"><tbody><tr style="font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="alert" style="font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #000; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #fff; margin: 0; padding: 20px;" align="center" valign="top" bgcolor="#fff"><br/>A Designing and development company</td></tr><tr><td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"><span style="font-family: sans-serif; font-weight: normal;">Hello</span><span style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;"><b>Admin,</b></span></p><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">You\'ve new Message;</p><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">Title: %ad_title%</p><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">Link: <a href="%ad_link%">%ad_title%</a></p><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">Sender: %sender_name%</p><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">Message: %message%</p><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"><strong>Thanks!</strong></p><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">ScriptsBundle</p></td></tr></tbody></table></td></tr></tbody></table><div class="footer" style="clear: both; padding-top: 10px; text-align: center; width: 100%;"><table style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" border="0" cellspacing="0" cellpadding="0"><tbody><tr><td class="content-block powered-by" style="font-family: sans-serif; font-size: 12px; vertical-align: top; color: #999999; text-align: center;"><a style="color: #999999; text-decoration: underline; font-size: 12px; text-align: center;" href="https://themeforest.net/user/scriptsbundle">Scripts Bundle</a>.</td></tr></tbody></table></div>&nbsp;</div></td><td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"></td></tr></tbody></table>&nbsp;',				
            ),
          array(
                'id'       => 'sb_report_ad_subject',
                'type'     => 'text',
                'title'    => __( 'Ad report email subject', 'adforest-rest-api' ),
                'desc'     => __( '%site_name% , %ad_title% will be translated accordingly.', 'adforest-rest-api' ),
                'default'  => 'Ad Reported - Adforest',				
            ),
          array(
                'id'       => 'sb_report_ad_from',
                'type'     => 'text',
                'title'    => __( 'Ad report email FROM', 'adforest-rest-api' ),
				'desc'     => __( 'FROM: NAME valid@email.com is compulsory as we gave in default.', 'adforest-rest-api' ),
                'default'  => 'From: '.get_bloginfo( 'name' ).' <'.get_option( 'admin_email' ).'>',				
            ),
          array(
                'id'       => 'sb_report_ad_message',
                'type'     => 'editor',
                'title'    => __( 'Ad Report template', 'adforest-rest-api' ),
                'desc'     => __( '%site_name% , %ad_owner% , %ad_title% , %ad_link% will be translated accordingly.', 'adforest-rest-api' ),
                'default'  => '<table class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #f6f6f6; width: 100%;" border="0" cellspacing="0" cellpadding="0"><tbody><tr><td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"></td><td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; max-width: 580px; padding: 10px; width: 580px; margin: 0 auto !important;"><div class="content" style="box-sizing: border-box; display: block; margin: 0 auto; max-width: 580px; padding: 10px;"><table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background: #fff; border-radius: 3px; width: 100%;"><tbody><tr><td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;"><table style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" border="0" cellspacing="0" cellpadding="0"><tbody><tr style="font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="alert" style="font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #000; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #fff; margin: 0; padding: 20px;" align="center" valign="top" bgcolor="#fff">A Designing and development company</td></tr><tr><td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"><span style="font-family: sans-serif; font-weight: normal;">Hello</span><span style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;"><b>Admin,</b></span></p><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">Below Ad is reported.</p><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">Title: %ad_title%</p><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">Link: <a href="%ad_link%">%ad_title%</a></p><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">Ad Poster: %ad_owner%</p><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"><strong>Thanks!</strong></p><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">ScriptsBundle</p></td></tr></tbody></table></td></tr></tbody></table><div class="footer" style="clear: both; padding-top: 10px; text-align: center; width: 100%;"><table style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" border="0" cellspacing="0" cellpadding="0"><tbody><tr><td class="content-block powered-by" style="font-family: sans-serif; font-size: 12px; vertical-align: top; color: #999999; text-align: center;"><a style="color: #999999; text-decoration: underline; font-size: 12px; text-align: center;" href="https://themeforest.net/user/scriptsbundle">Scripts Bundle</a>.</td></tr></tbody></table></div>&nbsp;</div></td><td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"></td></tr></tbody></table>&nbsp;',				
            ),
          array(
                'id'       => 'sb_forgot_password_subject',
                'type'     => 'text',
                'title'    => __( 'Reset Password email subject', 'adforest-rest-api' ),
                'desc'     => __( '%site_name% will be translated accordingly.', 'adforest-rest-api' ),
                'default'  => 'Reset Password - Adforest',				
            ),
          array(
                'id'       => 'sb_forgot_password_from',
                'type'     => 'text',
                'title'    => __( 'Reset Password email FROM', 'adforest-rest-api' ),
				'desc'     => __( 'FROM: NAME valid@email.com is compulsory as we gave in default.', 'adforest-rest-api' ),
                'default'  => get_bloginfo( 'name' ).' <'.get_option( 'admin_email' ).'>',				
            ),
          array(
                'id'       => 'sb_forgot_password_message',
                'type'     => 'editor',
                'title'    => __( 'Reset Password template', 'adforest-rest-api' ),
                'desc'     => __( '%site_name% , %user% , %reset_link% will be translated accordingly.', 'adforest-rest-api' ),
                'default'  => '<table class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #f6f6f6; width: 100%;" border="0" cellspacing="0" cellpadding="0"><tbody><tr><td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"></td><td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; max-width: 580px; padding: 10px; width: 580px; margin: 0 auto !important;"><div class="content" style="box-sizing: border-box; display: block; margin: 0 auto; max-width: 580px; padding: 10px;"><table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background: #fff; border-radius: 3px; width: 100%;"><tbody><tr><td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;"><table style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" border="0" cellspacing="0" cellpadding="0"><tbody><tr style="font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="alert" style="font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #000; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #fff; margin: 0; padding: 20px;" align="center" valign="top" bgcolor="#fff">A Designing and development company</td></tr><tr><td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"><span style="font-family: sans-serif; font-weight: normal;">Hello %user%</span><span style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;"><b>,</b></span></p>Please use this below link to reset your password.<br/>%reset_link%<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"><strong>Thanks!</strong></p><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">ScriptsBundle</p></td></tr></tbody></table></td></tr></tbody></table><div class="footer" style="clear: both; padding-top: 10px; text-align: center; width: 100%;"><table style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" border="0" cellspacing="0" cellpadding="0"><tbody><tr><td class="content-block powered-by" style="font-family: sans-serif; font-size: 12px; vertical-align: top; color: #999999; text-align: center;"><a style="color: #999999; text-decoration: underline; font-size: 12px; text-align: center;" href="https://themeforest.net/user/scriptsbundle">Scripts Bundle</a>.</td></tr></tbody></table></div>&nbsp;</div></td><td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"></td></tr></tbody></table>&nbsp;',				
            ),
          array(
                'id'       => 'sb_new_rating_subject',
                'type'     => 'text',
                'title'    => __( 'Rating email subject', 'adforest-rest-api' ),
                'desc'     => __( '%site_name% will be translated accordingly.', 'adforest-rest-api' ),
                'default'  => 'New Rating - Adforest',				
            ),
          array(
                'id'       => 'sb_new_rating_from',
                'type'     => 'text',
                'title'    => __( 'New rating email FROM', 'adforest-rest-api' ),
				'desc'     => __( 'FROM: NAME valid@email.com is compulsory as we gave in default.', 'adforest-rest-api' ),
                'default'  => 'From: '.get_bloginfo( 'name' ).' <'.get_option( 'admin_email' ).'>',				
            ),
          array(
                'id'       => 'sb_new_rating_message',
                'type'     => 'editor',
                'title'    => __( 'New rating template', 'adforest-rest-api' ),
                'desc'     => __( '%site_name% , %receiver% , %rator% , %rating% , %comments% , %rating_link% will be translated accordingly.', 'adforest-rest-api' ),
                'default'  => '<table class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #f6f6f6; width: 100%;" border="0" cellspacing="0" cellpadding="0"><tbody><tr><td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"></td><td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; max-width: 580px; padding: 10px; width: 580px; margin: 0 auto !important;"><div class="content" style="box-sizing: border-box; display: block; margin: 0 auto; max-width: 580px; padding: 10px;"><table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background: #fff; border-radius: 3px; width: 100%;"><tbody><tr><td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;"><table style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" border="0" cellspacing="0" cellpadding="0"><tbody><tr style="font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="alert" style="font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #000; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #fff; margin: 0; padding: 20px;" align="center" valign="top" bgcolor="#fff">A Designing and development company</td></tr><tr><td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"><span style="font-family: sans-serif; font-weight: normal;">Hello %receiver%</span><span style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;"><b>,</b></span></p>You got new rating;User who rated: %rator%Stars: %rating%Link: %rating_link%Comments: %comments%<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"><strong>Thanks!</strong></p><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">ScriptsBundle</p></td></tr></tbody></table></td></tr></tbody></table><div class="footer" style="clear: both; padding-top: 10px; text-align: center; width: 100%;"><table style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" border="0" cellspacing="0" cellpadding="0"><tbody><tr><td class="content-block powered-by" style="font-family: sans-serif; font-size: 12px; vertical-align: top; color: #999999; text-align: center;"><a style="color: #999999; text-decoration: underline; font-size: 12px; text-align: center;" href="https://themeforest.net/user/scriptsbundle">Scripts Bundle</a>.</td></tr></tbody></table></div>&nbsp;</div></td><td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"></td></tr></tbody></table>&nbsp;',				
            ),
          array(
                'id'       => 'sb_new_bid_subject',
                'type'     => 'text',
                'title'    => __( 'Bid email subject', 'adforest-rest-api' ),
                'desc'     => __( '%site_name% will be translated accordingly.', 'adforest-rest-api' ),
                'default'  => 'New Bid - Adforest',				
            ),
          array(
                'id'       => 'sb_new_bid_from',
                'type'     => 'text',
                'title'    => __( 'Bid email FROM', 'adforest-rest-api' ),
				'desc'     => __( 'FROM: NAME valid@email.com is compulsory as we gave in default.', 'adforest-rest-api' ),
                'default'  => 'From: '.get_bloginfo( 'name' ).' <'.get_option( 'admin_email' ).'>',				
            ),
          array(
                'id'       => 'sb_new_bid_message',
                'type'     => 'editor',
                'title'    => __( 'Bid email template', 'adforest-rest-api' ),
                'desc'     => __( '%site_name% , %receiver% , %bidder% , %bid% , %comments% , %bid_link% will be translated accordingly.', 'adforest-rest-api' ),
                'default'  => '<table class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #f6f6f6; width: 100%;" border="0" cellspacing="0" cellpadding="0"><tbody><tr><td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"></td><td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; max-width: 580px; padding: 10px; width: 580px; margin: 0 auto !important;"><div class="content" style="box-sizing: border-box; display: block; margin: 0 auto; max-width: 580px; padding: 10px;"><table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background: #fff; border-radius: 3px; width: 100%;"><tbody><tr><td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;"><table style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" border="0" cellspacing="0" cellpadding="0"><tbody><tr style="font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="alert" style="font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #000; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #fff; margin: 0; padding: 20px;" align="center" valign="top" bgcolor="#fff">A Designing and development company</td></tr><tr><td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"><span style="font-family: sans-serif; font-weight: normal;">Hello %receiver%</span><span style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;"><b>,</b></span></p>You got new Bid;Bidder: %bidder%Bid: %bid%Link: %bid_link%Comments: %comments%<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"><strong>Thanks!</strong></p><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">ScriptsBundle</p></td></tr></tbody></table></td></tr></tbody></table><div class="footer" style="clear: both; padding-top: 10px; text-align: center; width: 100%;"><table style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" border="0" cellspacing="0" cellpadding="0"><tbody><tr><td class="content-block powered-by" style="font-family: sans-serif; font-size: 12px; vertical-align: top; color: #999999; text-align: center;"><a style="color: #999999; text-decoration: underline; font-size: 12px; text-align: center;" href="https://themeforest.net/user/scriptsbundle">Scripts Bundle</a>.</td></tr></tbody></table></div>&nbsp;</div></td><td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"></td></tr></tbody></table>&nbsp;',				
            ),


          array(
                'id'       => 'sb_new_user_admin_message_subject',
                'type'     => 'text',
                'title'    => __( 'New user email template subject for Admin', 'adforest-rest-api' ),
                'default'  => 'New User Registration',				
            ),
          array(
                'id'       => 'sb_new_user_admin_message_from',
                'type'     => 'text',
                'title'    => __( 'New user email FROM for Admin', 'adforest-rest-api' ),
				'desc'     => __( 'NAME valid@email.com is compulsory as we gave in default.', 'adforest-rest-api' ),
                'default'  => get_bloginfo( 'name' ).' <'.get_option( 'admin_email' ).'>',				
            ),
          array(
                'id'       => 'sb_new_user_admin_message',
                'type'     => 'editor',
                'title'    => __( 'New user email template for Admin', 'adforest-rest-api' ),
                'desc'     => __( '%site_name% , %display_name%, %email% will be translated accordingly.', 'adforest-rest-api' ),
                'default'  => '<table class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #f6f6f6; width: 100%;" border="0" cellspacing="0" cellpadding="0"><tbody><tr><td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"></td><td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; max-width: 580px; padding: 10px; width: 580px; margin: 0 auto !important;"><div class="content" style="box-sizing: border-box; display: block; margin: 0 auto; max-width: 580px; padding: 10px;"><table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background: #fff; border-radius: 3px; width: 100%;"><tbody><tr><td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;"><table style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" border="0" cellspacing="0" cellpadding="0"><tbody><tr style="font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="alert" style="font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #000; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #fff; margin: 0; padding: 20px;" align="center" valign="top" bgcolor="#fff">A Designing and development company</td></tr><tr><td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"><span style="font-family: sans-serif; font-weight: normal;">Hello Admin</span><span style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;"><b>,</b></span></p>New user has registered on your site %site_name%;Name: %display_name%Email: %email%&nbsp;<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"><strong>Thanks!</strong></p><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">ScriptsBundle</p></td></tr></tbody></table></td></tr></tbody></table><div class="footer" style="clear: both; padding-top: 10px; text-align: center; width: 100%;"><table style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" border="0" cellspacing="0" cellpadding="0"><tbody><tr><td class="content-block powered-by" style="font-family: sans-serif; font-size: 12px; vertical-align: top; color: #999999; text-align: center;"><a style="color: #999999; text-decoration: underline; font-size: 12px; text-align: center;" href="https://themeforest.net/user/scriptsbundle">Scripts Bundle</a>.</td></tr></tbody></table></div>&nbsp;</div></td><td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"></td></tr></tbody></table>&nbsp;',				
            ),
			
			
          array(
                'id'       => 'sb_new_user_message_subject',
                'type'     => 'text',
                'title'    => __( 'New user email template subject', 'adforest-rest-api' ),
                'default'  => 'New User Registration',				
            ),
          array(
                'id'       => 'sb_new_user_message_from',
                'type'     => 'text',
                'title'    => __( 'New user email FROM', 'adforest-rest-api' ),
				'desc'     => __( 'NAME valid@email.com is compulsory as we gave in default.', 'adforest-rest-api' ),
                'default'  => get_bloginfo( 'name' ).' <'.get_option( 'admin_email' ).'>',				
            ),
          array(
                'id'       => 'sb_new_user_message',
                'type'     => 'editor',
                'title'    => __( 'New user email template', 'adforest-rest-api' ),
                'desc'     => __( '%site_name% , %user_name% %display_name% %verification_link% will be translated accordingly.', 'adforest-rest-api' ),
                'default'  => '<table class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #f6f6f6; width: 100%;" border="0" cellspacing="0" cellpadding="0"><tbody><tr><td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"></td><td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; max-width: 580px; padding: 10px; width: 580px; margin: 0 auto !important;"><div class="content" style="box-sizing: border-box; display: block; margin: 0 auto; max-width: 580px; padding: 10px;"><table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background: #fff; border-radius: 3px; width: 100%;"><tbody><tr><td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;"><table style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" border="0" cellspacing="0" cellpadding="0"><tbody><tr style="font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="alert" style="font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #000; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #fff; margin: 0; padding: 20px;" align="center" valign="top" bgcolor="#fff">A Designing and development company</td></tr><tr><td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"><span style="font-family: sans-serif; font-weight: normal;">Hello %display_name%</span><span style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;"><b>,</b></span></p>Welcome to %site_name%.<br/>Your details are below;<br/>Username: %user_name%<br/>&nbsp;<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"><strong>Thanks!</strong></p><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">ScriptsBundle</p></td></tr></tbody></table></td></tr></tbody></table><div class="footer" style="clear: both; padding-top: 10px; text-align: center; width: 100%;"><table style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" border="0" cellspacing="0" cellpadding="0"><tbody><tr><td class="content-block powered-by" style="font-family: sans-serif; font-size: 12px; vertical-align: top; color: #999999; text-align: center;"><a style="color: #999999; text-decoration: underline; font-size: 12px; text-align: center;" href="https://themeforest.net/user/scriptsbundle">Scripts Bundle</a>.</td></tr></tbody></table></div>&nbsp;</div></td><td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"></td></tr></tbody></table>&nbsp;',				
            ),
			
			
          array(
                'id'       => 'sb_active_ad_email_subject',
                'type'     => 'text',
                'title'    => __( 'Ad activation subject', 'adforest-rest-api' ),
                'default'  => 'You Ad has been activated.',				
            ),
          array(
                'id'       => 'sb_active_ad_email_from',
                'type'     => 'text',
                'title'    => __( 'Ad activation FROM', 'adforest-rest-api' ),
				'desc'     => __( 'NAME valid@email.com is compulsory as we gave in default.', 'adforest-rest-api' ),
                'default'  => get_bloginfo( 'name' ).' <'.get_option( 'admin_email' ).'>',				
            ),
          array(
                'id'       => 'sb_active_ad_email_message',
                'type'     => 'editor',
                'title'    => __( 'Ad activation message', 'adforest-rest-api' ),
                'desc'     => __( '%site_name% , %user_name%, %ad_title% ,  %ad_link% will be translated accordingly.', 'adforest-rest-api' ),
                'default'  => '<table class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #f6f6f6; width: 100%;" border="0" cellspacing="0" cellpadding="0"><tbody><tr><td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"></td><td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; max-width: 580px; padding: 10px; width: 580px; margin: 0 auto !important;"><div class="content" style="box-sizing: border-box; display: block; margin: 0 auto; max-width: 580px; padding: 10px;"><table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background: #fff; border-radius: 3px; width: 100%;"><tbody><tr><td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;"><table style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" border="0" cellspacing="0" cellpadding="0"><tbody><tr style="font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="alert" style="font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #000; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #fff; margin: 0; padding: 20px;" align="center" valign="top" bgcolor="#fff">A Designing and development company</td></tr><tr><td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"><span style="font-family: sans-serif; font-weight: normal;">Hello %user_name%</span><span style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;"><b>,</b></span></p><br/>You ad has been activated.<br/>Details are below;<br/>Ad Title: %ad_title%<br/>Ad Link: %ad_link%<br/>&nbsp;<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"><strong>Thanks!</strong></p><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">ScriptsBundle</p></td></tr></tbody></table></td></tr></tbody></table><div class="footer" style="clear: both; padding-top: 10px; text-align: center; width: 100%;"><table style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" border="0" cellspacing="0" cellpadding="0"><tbody><tr><td class="content-block powered-by" style="font-family: sans-serif; font-size: 12px; vertical-align: top; color: #999999; text-align: center;"><a style="color: #999999; text-decoration: underline; font-size: 12px; text-align: center;" href="https://themeforest.net/user/scriptsbundle">Scripts Bundle</a>.</td></tr></tbody></table></div>&nbsp;</div></td><td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"></td></tr></tbody></table>&nbsp;',				
            ),



	  array(
                'id'       => 'ad_rating_email_subject',
                'type'     => 'text',
                'title'    => __( 'Rating email subject', 'adforest-rest-api' ),
                'default'  => 'You have a new rating',				
            ),
          array(
                'id'       => 'ad_rating_email_from',
                'type'     => 'text',
                'title'    => __( 'Rating FROM', 'adforest-rest-api' ),
				'desc'     => __( 'NAME valid@email.com is compulsory as we gave in default.', 'adforest-rest-api' ),
                'default'  => get_bloginfo( 'name' ).' <'.get_option( 'admin_email' ).'>',				
            ),
          array(
                'id'       => 'ad_rating_email_message',
                'type'     => 'editor',
                'title'    => __( 'Rating message', 'adforest-rest-api' ),
				'args'   => array(
						'teeny'            => true,
						'textarea_rows'    => 10,
						'wpautop' => false,
					),
                'desc'     => '%site_name%, %ad_title%, %ad_link%, %rating, %rating_comments%, %author_name%'.  __( 'will be translated accordingly.', 'adforest-rest-api' ),
                'default'  => '<table class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #f6f6f6; width: 100%;" border="0" cellspacing="0" cellpadding="0"><tbody> <tr> <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"></td> <td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; max-width: 580px; padding: 10px; width: 580px; margin: 0 auto !important;"> <div class="content" style="box-sizing: border-box; display: block; margin: 0 auto; max-width: 580px; padding: 10px;"> <table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background: #fff; border-radius: 3px; width: 100%;"> <tbody> <tr> <td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;"> <table style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" border="0" cellspacing="0" cellpadding="0"> <tbody> <tr style="font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"> <td class="alert" style="font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #000; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #fff; margin: 0; padding: 20px;" align="center" valign="top" bgcolor="#fff"> A Designing and development company</td></tr><tr><td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"><span style="font-family: sans-serif; font-weight: normal;">Hello %author_name%</span><span style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;"><b>,</b></span></p> <br />You have new rating, details are below; <br /> Rating: %rating% <br />Comments: %rating_comments% <br /> Ad Title: %ad_title% <br /> Ad Link: %ad_link% <br />&nbsp;<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"><strong>Thanks!</strong></p><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">ScriptsBundle</p></td></tr></tbody></table></td></tr></tbody></table><div class="footer" style="clear: both; padding-top: 10px; text-align: center; width: 100%;"><table style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" border="0" cellspacing="0" cellpadding="0"><tbody><tr><td class="content-block powered-by" style="font-family: sans-serif; font-size: 12px; vertical-align: top; color: #999999; text-align: center;"><a style="color: #999999; text-decoration: underline; font-size: 12px; text-align: center;" href="https://themeforest.net/user/scriptsbundle">Scripts Bundle</a>.</td></tr></tbody></table></div>&nbsp; </div></td><td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"></td></tr></tbody></table>&nbsp;', ),
			
			
		  array(
                'id'       => 'ad_rating_reply_email_subject',
                'type'     => 'text',
                'title'    => __( 'Rating reply email subject', 'adforest-rest-api' ),
                'default'  => 'You got a reply on your rating',				
            ),
          array(
                'id'       => 'ad_rating_reply_email_from',
                'type'     => 'text',
                'title'    => __( 'Rating reply FROM', 'adforest-rest-api' ),
				'desc'     => __( 'NAME valid@email.com is compulsory as we gave in default.', 'adforest-rest-api' ),
                'default'  => get_bloginfo( 'name' ).' <'.get_option( 'admin_email' ).'>',				
            ),
          array(
                'id'       => 'ad_rating_reply_email_message',
                'type'     => 'editor',
                'title'    => __( 'Rating reply message', 'adforest-rest-api' ),
				'args'   => array(
						'teeny'            => true,
						'textarea_rows'    => 10,
						'wpautop' => false,
					),
               'desc'     => '%site_name%, %ad_title%, %ad_link%, %rating%, %rating_comments%, %author_name%, %author_reply% '. __( 'will be translated accordingly.', 'adforest-rest-api' ),
                'default'  => '<table class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #f6f6f6; width: 100%;" border="0" cellspacing="0" cellpadding="0"> <tbody> <tr> <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"></td> <td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; max-width: 580px; padding: 10px; width: 580px; margin: 0 auto !important;"> <div class="content" style="box-sizing: border-box; display: block; margin: 0 auto; max-width: 580px; padding: 10px;"> <table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background: #fff; border-radius: 3px; width: 100%;"> <tbody><tr><td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;"><table style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" border="0" cellspacing="0" cellpadding="0"> <tbody> <tr style="font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"> <td class="alert" style="font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #000; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #fff; margin: 0; padding: 20px;" align="center" valign="top" bgcolor="#fff"> A Designing and development company</td></tr><tr><td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"> <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"><span style="font-family: sans-serif; font-weight: normal;">Hello,</span><span style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;"><b>,</b></span></p> <br /> You have reply on your rating, details are below; <br /> Ad Title: %ad_title% <br />Ad Link: %ad_link% <br /> Ad Author: %author_name% <br />Author reply: %author_reply% <br />Your given rating: %rating% <br />Your comments: %rating_comments% <br />&nbsp;<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"><strong>Thanks!</strong></p><p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">ScriptsBundle</p></td></tr></tbody></table></td></tr></tbody></table><div class="footer" style="clear: both; padding-top: 10px; text-align: center; width: 100%;"><table style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" border="0" cellspacing="0" cellpadding="0"><tbody><tr><td class="content-block powered-by" style="font-family: sans-serif; font-size: 12px; vertical-align: top; color: #999999; text-align: center;"><a style="color: #999999; text-decoration: underline; font-size: 12px; text-align: center;" href="https://themeforest.net/user/scriptsbundle">Scripts Bundle</a>.</td></tr></tbody></table></div>&nbsp;</div></td><td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"></td></tr></tbody></table>&nbsp;',),








        )
    ) );	