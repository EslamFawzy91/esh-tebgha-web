<?php
/* ----
	Settings Starts Here
----*/
add_action( 'rest_api_init', 'adforestAPI_settings_api_hooks_get', 0 );
function adforestAPI_settings_api_hooks_get() {

    register_rest_route( 'adforest/v1', '/settings/',
        array(
				'methods'  => WP_REST_Server::READABLE,
				'callback' => 'adforestAPI_settings_me_get',
				/*'permission_callback' => function () { return adforestAPI_basic_auth();  },*/
        	)
    );
}

if( !function_exists('adforestAPI_settings_me_get' ) )
{
	function adforestAPI_settings_me_get()
	{
		global $adforestAPI;
		
		
		
		$app_is_open = (isset( $adforestAPI['app_is_open'] ) && $adforestAPI['app_is_open'] == true) ? true : false;
		$data['is_app_open']					= $app_is_open;
		$data['heading']						= __("Register With Us!", "adforest-rest-api");		
		$data['internet_dialog']['title']		= __("Error", "adforest-rest-api");
		$data['internet_dialog']['text']		= __("Internet not found", "adforest-rest-api");
		$data['internet_dialog']['ok_btn']		= __("Ok", "adforest-rest-api");
		$data['internet_dialog']['cancel_btn']	= __("Cancel", "adforest-rest-api");
		
		$data['alert_dialog']['message']		= __("Are you sure you want to do this?", "adforest-rest-api");
		$data['alert_dialog']['title']			= __("Alert!", "adforest-rest-api");
		
		$data['search']['text']					=  __("Search Here", "adforest-rest-api");
		$data['search']['input']				=  'ad_title';/*Static name For field name*/		
		$data['cat_input']						=  'ad_cats1';/*Static name For categories*/
		$data['message']						= __("Please wait!", "adforest-rest-api");
		
		/*Options Coming From Theme Options*/
		
		$gmap_lang 				= (isset( $adforestAPI['gmap_lang'] ) && $adforestAPI['gmap_lang'] == true) ? $adforestAPI['gmap_lang'] : 'en';
		$data['gmap_lang']		= $gmap_lang;

		$is_rtl 				= (isset( $adforestAPI['app_settings_rtl'] ) && $adforestAPI['app_settings_rtl'] == true) ? true : false;
		$data['is_rtl']			= $is_rtl;

		$app_color  			= (isset( $adforestAPI['app_settings_color'] ) ) ? $adforestAPI['app_settings_color'] : '#f58936';
		$data['main_color']		= $app_color;

		$sb_location_type  			= (isset( $adforestAPI['sb_location_type'] ) ) ? $adforestAPI['sb_location_type'] : 'cities';
		$data['location_type']		= $sb_location_type;

		/*Some App Keys From Theme Options 
		$data['appKey']['stripe']		= (isset( $adforestAPI['appKey_stripeKey'] ) ) ? $adforestAPI['appKey_stripeKey'] : '';		
		$data['appKey']['paypal']		= (isset( $adforestAPI['appKey_paypalKey'] ) ) ? $adforestAPI['appKey_paypalKey'] : '';*/
		
		$data['registerBtn_show']['google'] = (isset( $adforestAPI['app_settings_google_btn'] ) && $adforestAPI['app_settings_google_btn'] == true ) ? true : false;
		$data['registerBtn_show']['facebook']	= (isset( $adforestAPI['app_settings_fb_btn'] ) && $adforestAPI['app_settings_fb_btn'] == true ) ? true : false;
		
		
		$data['dialog']['confirmation']	=  array(	
			"title" => __("Confirmation", "adforest-rest-api"),
			"text" => __("Are you sure you want to do this.", "adforest-rest-api"),
			"btn_no" => __("Cancel", "adforest-rest-api"),
			"btn_ok" => __("Confirm", "adforest-rest-api"),
		);
		
		$data['notLogin_msg']	= __("Please login to perform this action.", "adforest-rest-api");
		
		$enable_featured_slider_scroll = (isset( $adforestAPI['sb_enable_featured_slider_scroll'] ) && $adforestAPI['sb_enable_featured_slider_scroll'] == true ) ? true : false;
		
		$data['featured_scroll_enabled'] = $enable_featured_slider_scroll;
		if($enable_featured_slider_scroll)
		{
			$data['featured_scroll']['duration'] = (isset( $adforestAPI['sb_enable_featured_slider_duration'] ) && $adforestAPI['sb_enable_featured_slider_duration'] == true ) ? $adforestAPI['sb_enable_featured_slider_duration'] : 40;
			
			$data['featured_scroll']['loop'] = (isset( $adforestAPI['sb_enable_featured_slider_loop'] ) && $adforestAPI['sb_enable_featured_slider_loop'] == true ) ? $adforestAPI['sb_enable_featured_slider_loop'] : 2000;
		}
		
		$data['location_popup']['slider_number'] = 250;
		$data['location_popup']['slider_step'] = 5;
		$data['location_popup']['text'] = __("Select distance in (KM)", "adforest-rest-api");
		$data['location_popup']['btn_submit'] = __("Submit", "adforest-rest-api");
		$data['location_popup']['btn_clear'] = __("Clear", "adforest-rest-api");

		/*App GPS Section Starts */
		$allow_near_by = (isset( $adforestAPI['allow_near_by'] ) && $adforestAPI['allow_near_by'] ) ? true : false;
		$data['show_nearby'] = $allow_near_by;
		$data['gps_popup']['title'] = __("GPS Settings", "adforest-rest-api");
		$data['gps_popup']['text'] = __("GPS is not enabled. Do you want to go to settings menu?", "adforest-rest-api");
		$data['gps_popup']['btn_confirm'] = __("Settings", "adforest-rest-api");
		$data['gps_popup']['btn_cancel'] = __("Cancel", "adforest-rest-api");
		/*App GPS Section Ends */

		/*App Rating Section Starts */
		$allow_app_rating 		= (isset( $adforestAPI['allow_app_rating'] ) && $adforestAPI['allow_app_rating'] ) ? true : false;
		
		$allow_app_rating_title 	= (isset( $adforestAPI['allow_app_rating_title'] ) && $adforestAPI['allow_app_rating_title'] != "" ) ? $adforestAPI['allow_app_rating_title'] : __("App Store Rating", "adforest-rest-api");
		
		$allow_app_rating_url 	= (isset( $adforestAPI['allow_app_rating_url'] ) && $adforestAPI['allow_app_rating_url'] != "" ) ? $adforestAPI['allow_app_rating_url'] : '';
				
		$data['app_rating']['is_show'] 		= $allow_app_rating;
		$data['app_rating']['title'] 		= $allow_app_rating_title;
		
		$data['app_rating']['btn_confirm'] 	= __("Maybe Later", "adforest-rest-api");
		$data['app_rating']['btn_cancel'] 	= __("Never", "adforest-rest-api");
		$data['app_rating']['url'] 			= $allow_app_rating_url;
		/*App Rating Section Ends */


		/*App Share Section Starts */
		$allow_app_share = (isset( $adforestAPI['allow_app_share'] ) && $adforestAPI['allow_app_share'] ) ? true : false;		
		$allow_app_share_title 	= (isset( $adforestAPI['allow_app_share_title'] ) && $adforestAPI['allow_app_share_title'] != "" ) ? $adforestAPI['allow_app_share_title'] : __("Share this", "adforest-rest-api");
		$allow_app_share_text 	= (isset( $adforestAPI['allow_app_share_text'] ) && $adforestAPI['allow_app_share_text'] != "" ) ? $adforestAPI['allow_app_share_text'] : '';
		$allow_app_share_url = (isset( $adforestAPI['allow_app_share_url'] ) && $adforestAPI['allow_app_share_url'] != "" ) ? $adforestAPI['allow_app_share_url'] : '';
		
		$data['app_share']['is_show'] 		= $allow_app_share;
		$data['app_share']['title'] 		= $allow_app_share_title;
		$data['app_share']['text'] 			= $allow_app_share_text;
		$data['app_share']['url'] 			= $allow_app_share_url;
		/*App Share Section Ends */
		
		$sb_user_guest_dp = ADFOREST_API_PLUGIN_URL."images/user.jpg";
		if( adforestAPI_getReduxValue('sb_user_guest_dp', 'url', true) )
		{
			$sb_user_guest_dp = adforestAPI_getReduxValue('sb_user_guest_dp', 'url', false);	
		}

		$data['guest_image'] = $sb_user_guest_dp;
		$data['guest_name']  = __("Guest", "adforest-rest-api");
		
		
		$has_value = false;
		$array_sortable = array();
		if(isset( $adforestAPI['home-screen-sortable'] ) && $adforestAPI['home-screen-sortable'] > 0 )
		{
			
			$array_sortable = $adforestAPI['home-screen-sortable'];
			foreach( $array_sortable as $key => $val )
			{
				if( isset($val)  && $val != "" )
				{
					$has_value = true;
				}
			}
		}
		$data['ads_position_sorter'] =  $has_value;

		$data['menu'] 	= adforestAPI_appMenu_settings();
		
		
		
		
		
		
		$data['messages_screen']['main_title'] 		= __("Messages", "adforest-rest-api");
		$data['messages_screen']['sent'] 			= __("Sent Offers", "adforest-rest-api");
		$data['messages_screen']['receive']			= __("Offers on Ads", "adforest-rest-api");
				
				
	$data['gmap_has_countries'] = false;
	if( isset( $adforestAPI['sb_location_allowed'] ) && $adforestAPI['sb_location_allowed'] == false && isset ($adforestAPI['sb_list_allowed_country'] ) )
	{
		$data['gmap_has_countries'] = true;
		$lists = $adforestAPI['sb_list_allowed_country'];
		/*$countries = array(); foreach( $lists as $list ) { $countries[] = $list; }*/
		$data['gmap_countries'] = $lists;
	}				
			
			
		$data['app_show_languages'] = false;		
		$languages = array();	
		/*$languages[] = array("key" => "en", "value" => "English", "is_rtl" => false);
		$languages[] = array("key" => "ar", "value" => "Arabic", "is_rtl" => true);
		$languages[] = array("key" => "ro_RO", "value" => "RO Lang", "is_rtl" => false);*/
		if( count($languages) > 0 )
		{
			$data['app_text_title'] = __("Select or Search Language", "adforest-rest-api");	
			$data['app_text_close'] = __("Close", "adforest-rest-api");	
			$data['app_show_languages'] = true;	
			$data['app_languages'] = $languages;	
		}	
	
		$data['allow_block'] = (isset( $adforestAPI['sb_user_allow_block'] ) && $adforestAPI['sb_user_allow_block']) ? true : false;		
		return $response = array( 'success' => true, 'data' => $data, 'message'  => ''  );		
		
	}
}

if( !function_exists('adforestAPI_is_app_open' ) )
{
	function adforestAPI_is_app_open()
	{
		global $adforestAPI;
		
		$app_is_open = (isset( $adforestAPI['app_is_open'] ) && $adforestAPI['app_is_open'] == true) ? true : false;
		$data['is_app_open']					= $app_is_open;
	}
}